package com.app.connection;


public class WS_Request_Constants {

	public static boolean VAL_TRUE = true;
	public static boolean VAL_FALSE = false;	

	class CommonFields{
		public final static String KEY_ACTION = "action";
	}

	/**
	 * Login Instances
	 */
	class Action_Login{
		public final static String KEY_PARAM_email = "email";
		public final static String KEY_PARAM_password = "password";
	}
	
	/**
	 * Logout Instances
	 */
	class Action_Logout{
		public final static String KEY_PARAM_key = "key";
	}
}
