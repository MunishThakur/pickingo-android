package com.app.connection;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.app.database.DatabaseManager;
import com.app.objects.Object_Final_Request;
import com.app.objects.Object_Header;
import com.app.objects.Object_Login;
import com.app.objects.Object_Pickup_Request;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class WS_Calls {

	private static final String PARAM_FILE = "signature";

	/**
	 * Service Params
	 */
	public Object_Login ws_GetLoginAuth(final String s_user_name, final String s_user_password) {

		final String URL = "http://api.pickingo.com/pb_auth/sign_in";

		HashMap<String, String> hm_params = new HashMap<String, String>();
		hm_params.put(WS_Request_Constants.Action_Login.KEY_PARAM_email, s_user_name);
		hm_params.put(WS_Request_Constants.Action_Login.KEY_PARAM_password, s_user_password);

		Object_Login response = getResoponse_POST_LOGIN(URL, hm_params);

		return response;
	}

	public List<Object_Pickup_Request> ws_GetRequests(String id, Object_Header object_Header) {
		final String URL = "http://api.pickingo.com/api/v1/pickup_boys/" + id + "/requests";
		return getResponse_Get(URL, object_Header);
	}

	public Object_Pickup_Request ws_PutUpdateRequests(Object_Login object_Login, Object_Final_Request final_Request) {
		String userId = object_Login.getData().getId();
		Object_Header object_Header = object_Login.getObj_header();
		String jsonString = DatabaseManager.Object_GsonString(final_Request);
		int reqId = final_Request.getId();
		final String URL = "http://api.pickingo.com/api/v1/pickup_boys/" + userId + "/requests/" + reqId;
		return getResoponse_PUT(URL, object_Header, jsonString);
	}

	public Object_Pickup_Request ws_PutUpdateSignRequests(Object_Login object_Login, Object_Final_Request final_Request, File sign_file) {
		String userId = object_Login.getData().getId();
		Object_Header object_Header = object_Login.getObj_header();
		// String jsonString = DatabaseManager.Object_GsonString(final_Request);
		int reqId = final_Request.getId();
		final String URL = "http://api.pickingo.com/api/v1/pickup_boys/" + userId + "/requests/" + reqId;
		return getResoponse_PUT_Image_File(URL, object_Header, PARAM_FILE, sign_file);
	}

	public String ws_GetLogout(final String key) {

		final String URL = "";

		HashMap<String, String> hm_params = new HashMap<String, String>();
		hm_params.put(WS_Request_Constants.Action_Logout.KEY_PARAM_key, key);

		String response = getResoponse_POST(URL, hm_params);

		return response;
	}

	/*
	 * public String ws_uploadPhoto_To_Server_Photos(String key, String id,
	 * final File uploadedfile){
	 * 
	 * String URL =
	 * "http://mazkara.ajwa.me/api/v1?action=businesses%2Fphotos%2Fupdate&id="
	 * +id;
	 * 
	 * HashMap<String, String> hm_params = new HashMap<String, String>();
	 * hm_params
	 * .put(WS_Request_Constants.Action_UploadImages_Photos.KEY_PARAM_key, key);
	 * 
	 * String response = "";
	 * 
	 * if(uploadedfile != null){ response = getResoponse_POST(URL, hm_params,
	 * WS_Request_Constants.Action_UploadImages_Photos.KEY_PARAM_UPLOAD_FILE,
	 * uploadedfile);
	 * 
	 * }
	 * 
	 * return response; }
	 * 
	 * public String ws_uploadPhoto_To_Server_RateCards(String key, String id,
	 * final File uploadedfile){
	 * 
	 * String URL =
	 * "http://mazkara.ajwa.me/api/v1?action=businesses%2Frate_cards%2Fupdate&id="
	 * +id;
	 * 
	 * HashMap<String, String> hm_params = new HashMap<String, String>();
	 * hm_params
	 * .put(WS_Request_Constants.Action_UploadImages_Photos.KEY_PARAM_key, key);
	 * 
	 * String response = "";
	 * 
	 * if(uploadedfile != null){ response = getResoponse_POST(URL, hm_params,
	 * WS_Request_Constants.Action_UploadImages_RateCard.KEY_PARAM_UPLOAD_FILE,
	 * uploadedfile); }
	 * 
	 * return response; }
	 */
	public String ws_GetEvents() {

		String response = "";

		return response;
	}

	// ***************************************************************************************************************************************
	// //
	// ***************************************************************************************************************************************
	// //
	// ******************************************** MAIN METHODS --- START
	// *************************************************************** //
	// ***************************************************************************************************************************************
	// //
	// ***************************************************************************************************************************************
	// //

	private String getResoponse_GET(final String URL) {

		String response = "";

		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();

		try {

			HttpGet request = new HttpGet();
			URI website = new URI(URL);
			request.setURI(website);
			HttpResponse http_response = httpclient.execute(request);
			InputStream is = http_response.getEntity().getContent();

			response = convertStreamToString(is);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}

		return response;
	}

	private List<Object_Pickup_Request> getResponse_Get(String URL, Object_Header object_Header) {
		String response = "";
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		List<Object_Pickup_Request> list_Object_Pickup_Requests = null;
		try {
			HttpGet request = new HttpGet();
			// setting up request header
			setHeader(request, object_Header);
			URI website = new URI(URL);
			request.setURI(website);
			HttpResponse http_response = httpclient.execute(request);
			InputStream is = http_response.getEntity().getContent();
			response = convertStreamToString(is);
			// new Gson().fro

			Gson gson = new Gson();
			Type collectionType = new TypeToken<List<Object_Pickup_Request>>() {
			}.getType();
			list_Object_Pickup_Requests = gson.fromJson(response, collectionType);

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list_Object_Pickup_Requests;
	}

	private void setHeader(HttpGet request, Object_Header object_Header) {
		request.addHeader(Object_Header.KEY_access_token, object_Header.getAccess_token());
		request.addHeader(Object_Header.KEY_client, object_Header.getClient());
		request.addHeader(Object_Header.KEY_token_type, object_Header.getToken_type());
		request.addHeader(Object_Header.KEY_uid, object_Header.getUid());
	}

	private String getResoponse_POST(final String URL) {

		String response = "";

		try {
			// Create a new HttpClient and Post Header
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(URL);

			// Execute HTTP Post Request
			HttpResponse httpResponse = httpclient.execute(httppost);

			HttpEntity entity = httpResponse.getEntity();
			InputStream is = entity.getContent();

			response = convertStreamToString(is);
		} catch (Exception e) {
			System.out.println();
		}

		return response;
	}

	private Object_Login getResoponse_POST_LOGIN(final String URL, final HashMap<String, String> hm_params) {

		Object_Login obj_login = new Object_Login();
		String response = "";

		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(URL);

		try {

			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

			Set set_params = hm_params.keySet();
			Iterator itr_params = set_params.iterator();

			while (itr_params.hasNext()) {
				String key = itr_params.next().toString();
				String value = hm_params.get(key);
				nameValuePairs.add(new BasicNameValuePair(key, value));
			}

			if ((nameValuePairs != null) && (nameValuePairs.size() > 0)) {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			}

			// Execute HTTP Post Request
			HttpResponse httpResponse = httpclient.execute(httppost);

			Header[] array_headers = httpResponse.getAllHeaders();
			HashMap<String, String> hm_header = convertHeadersToHashMap(array_headers);

			Object_Header obj_header = new Object_Header();
			if (hm_header != null) {
				obj_header.setUid(hm_header.get(obj_header.KEY_uid));
				obj_header.setAccess_token(hm_header.get(obj_header.KEY_access_token));
				obj_header.setClient(hm_header.get(obj_header.KEY_client));
				obj_header.setToken_type(hm_header.get(obj_header.KEY_token_type));
			}

			for (int index = 0; index < array_headers.length; index++) {

			}
			HttpEntity entity = httpResponse.getEntity();
			InputStream is = entity.getContent();

			response = convertStreamToString(is);

			obj_login = new Gson().fromJson(response, Object_Login.class);
			if (obj_login != null) {
				obj_login.setObj_header(obj_header);
			}
		} catch (Exception e) {
			System.out.println();
		}
		return obj_login;
	}

	private HashMap<String, String> convertHeadersToHashMap(Header[] headers) {
		HashMap<String, String> hm_header = new HashMap<String, String>();
		for (Header header : headers) {
			hm_header.put(header.getName(), header.getValue());
		}
		return hm_header;
	}

	private String getResoponse_POST(final String URL, final HashMap<String, String> hm_params) {

		String response = "";

		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(URL);

		try {

			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

			Set set_params = hm_params.keySet();
			Iterator itr_params = set_params.iterator();

			while (itr_params.hasNext()) {
				String key = itr_params.next().toString();
				String value = hm_params.get(key);
				nameValuePairs.add(new BasicNameValuePair(key, value));
			}

			if ((nameValuePairs != null) && (nameValuePairs.size() > 0)) {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			}

			// Execute HTTP Post Request
			HttpResponse httpResponse = httpclient.execute(httppost);

			HttpEntity entity = httpResponse.getEntity();
			InputStream is = entity.getContent();

			response = convertStreamToString(is);
		} catch (Exception e) {
			System.out.println();
		}

		return response;
	}

	private Object_Pickup_Request getResoponse_PUT(final String URL, Object_Header object_Header, final String hm_params) {

		String response = "";
		Object_Pickup_Request list_Object_Pickup_Requests = null;
		// Create a new HttpClient and Post Header
		String type = null;

		// setting up headers
		HttpClient httpclient = new DefaultHttpClient();
		HttpPut httpPut = new HttpPut(URL);
		httpPut.addHeader(Object_Header.KEY_access_token, object_Header.getAccess_token());
		httpPut.addHeader(Object_Header.KEY_client, object_Header.getClient());
		httpPut.addHeader(Object_Header.KEY_token_type, object_Header.getToken_type());
		httpPut.addHeader(Object_Header.KEY_uid, object_Header.getUid());
		httpPut.addHeader("Content-Type", "application/json");

		try {

			StringEntity stringEntity = new StringEntity(hm_params);

			// MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			//
			// builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

			// final HttpEntity yourEntity = builder.build();
			// if (img_file != null) {
			// // builder.addPart(PARAM_file, new FileBody(img_file));
			// builder.addBinaryBody(PARAM_FILE, img_file,
			// ContentType.create("image/png"), img_file.getName());
			// }

			httpPut.setEntity(stringEntity);
			// Execute HTTP Put Request
			HttpResponse httpResponse = httpclient.execute(httpPut);

			HttpEntity entity = httpResponse.getEntity();
			InputStream is = entity.getContent();

			response = convertStreamToString(is);
			Gson gson = new Gson();
			list_Object_Pickup_Requests = gson.fromJson(response, Object_Pickup_Request.class);

			System.out.println();
		} catch (Exception e) {
			System.out.println();
		}

		return list_Object_Pickup_Requests;
	}

	// munish code
	public Object_Pickup_Request getResoponse_PUT_Image_File(final String URL, Object_Header object_Header, final String PARAM_file, final File img_file) {

		String response = "";

		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPut httpput = new HttpPut(URL);

		// setting up object header
		httpput.addHeader(Object_Header.KEY_access_token, object_Header.getAccess_token());
		httpput.addHeader(Object_Header.KEY_client, object_Header.getClient());
		httpput.addHeader(Object_Header.KEY_token_type, object_Header.getToken_type());
		httpput.addHeader(Object_Header.KEY_uid, object_Header.getUid());
//		 httpput.addHeader("Content-Type", "multipart/form-data");

		try {

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();

			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

			// Set set_params = hm_params.keySet();
			// Iterator itr_params = set_params.iterator();
			//
			// while(itr_params.hasNext()){
			// String key = itr_params.next().toString();
			// String value = hm_params.get(key);
			//
			// //nameValuePairs.add(new BasicNameValuePair(key, value));
			// builder.addTextBody(key, value);
			// //multi_entity.addPart(key, new StringBody(value));
			// }

			// if((nameValuePairs != null) && (nameValuePairs.size() > 0)){
			// httpput.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			// }

			if (img_file != null) {
				/*
				 * ByteArrayOutputStream bos = new ByteArrayOutputStream();
				 * bitmap.compress(CompressFormat.JPEG, 100, bos); byte[] data =
				 * bos.toByteArray();
				 * 
				 * ByteArrayBody bab = new ByteArrayBody(data, fileName+".jpg");
				 * 
				 * MultipartEntity reqEntity = new
				 * MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
				 * reqEntity.addPart("userfile", bab);
				 * httppost.setEntity(reqEntity);
				 */
				ContentType contentType = ContentType.create("image/png", "utf-8");

				builder.addPart(PARAM_file, new FileBody(img_file, contentType, img_file.getName()));
			}

			httpput.setEntity(builder.build());
			// Execute HTTP Post Request
			HttpResponse httpResponse = httpclient.execute(httpput);

			HttpEntity entity = httpResponse.getEntity();
			InputStream is = entity.getContent();

			response = convertStreamToString(is);
			Gson gson = new Gson();
			Object_Pickup_Request object_Pickup_Requests = gson.fromJson(response, Object_Pickup_Request.class);

			System.out.println();
			return object_Pickup_Requests;
		} catch (Exception e) {
			System.out.println();
		}

		return null;
	}

	private Object_Pickup_Request getResoponse_Sign_PUT(final String URL, Object_Header object_Header, final File file_sign) {

		String response = "";
		Object_Pickup_Request list_Object_Pickup_Requests = null;
		// Create a new HttpClient and Post Header
		String type = null;

		// setting up headers
		HttpClient httpclient = new DefaultHttpClient();
		HttpPut httpPut = new HttpPut(URL);
		httpPut.addHeader(Object_Header.KEY_access_token, object_Header.getAccess_token());
		httpPut.addHeader(Object_Header.KEY_client, object_Header.getClient());
		httpPut.addHeader(Object_Header.KEY_token_type, object_Header.getToken_type());
		httpPut.addHeader(Object_Header.KEY_uid, object_Header.getUid());
		httpPut.addHeader("Content-Type", "application/octet-stream");

		try {

			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			Bitmap bitmap = BitmapFactory.decodeFile(file_sign.getAbsolutePath(), options);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
			byte[] imageBytes = baos.toByteArray();

			// ContentBody contentPart = new ByteArrayBody(imageBytes,
			// "image/png", "pickingo_android.png");
			// ByteArrayBody bab = new ByteArrayBody(imageBytes,
			// "pickingo_android.png");

			// StringEntity stringEntity = new StringEntity(hm_params);
			ContentBody contentPart = new ByteArrayBody(imageBytes, "pickingo_android.png");
			// MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			//
			// builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			// final HttpEntity yourEntity = builder.build();
			// if (file_sign != null) {
			// builder.addPart(PARAM_FILE, contentPart);
			//
			// // builder.addBinaryBody(PARAM_FILE, file_sign,
			// ContentType.create("image/png"), file_sign.getName());
			// }

			// StringEntity stringEntity = new StringEntity(hm_params);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();

			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

			// final HttpEntity yourEntity = builder.build();
			// if (file_sign != null) {
			// builder.addPart(PARAM_FILE, new FileBody(file_sign));
			// // builder.addBinaryBody(PARAM_FILE, sign_file,
			// ContentType.create("image/png", file_sign.getName());
			// }

			if (file_sign != null)
				builder.addBinaryBody(PARAM_FILE, file_sign, ContentType.APPLICATION_OCTET_STREAM, file_sign.getName());

			FileEntity fileEntity = new FileEntity(file_sign, "image/png");
			httpPut.setEntity(fileEntity);
			// Execute HTTP Put Request
			HttpResponse httpResponse = httpclient.execute(httpPut);

			HttpEntity entity = httpResponse.getEntity();
			InputStream is = entity.getContent();

			response = convertStreamToString(is);
			Gson gson = new Gson();
			list_Object_Pickup_Requests = gson.fromJson(response, Object_Pickup_Request.class);

			System.out.println();
		} catch (Exception e) {
			System.out.println();
		}

		return list_Object_Pickup_Requests;
	}

	private String getResoponse_POST(final String URL, final HashMap<String, String> hm_params, List<NameValuePair> nameValuePairs) {

		String response = "";

		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(URL);

		try {

			// Add your data
			Set set_params = hm_params.keySet();
			Iterator itr_params = set_params.iterator();

			while (itr_params.hasNext()) {
				String key = itr_params.next().toString();
				String value = hm_params.get(key);
				if (value != null && value.trim().length() > 0) {
					nameValuePairs.add(new BasicNameValuePair(key, value));
				}
			}

			if ((nameValuePairs != null) && (nameValuePairs.size() > 0)) {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			}

			// Execute HTTP Post Request
			HttpResponse httpResponse = httpclient.execute(httppost);

			HttpEntity entity = httpResponse.getEntity();
			InputStream is = entity.getContent();

			response = convertStreamToString(is);
		} catch (Exception e) {
			System.out.println();
		}

		return response;
	}

	private String getResoponse_POST(final String URL, final HashMap<String, String> hm_params, final String PARAM_file, final File img_file) {

		String response = "";

		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(URL);

		try {

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();

			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

			Set set_params = hm_params.keySet();
			Iterator itr_params = set_params.iterator();

			while (itr_params.hasNext()) {
				String key = itr_params.next().toString();
				String value = hm_params.get(key);

				// nameValuePairs.add(new BasicNameValuePair(key, value));
				builder.addTextBody(key, value);
				// multi_entity.addPart(key, new StringBody(value));
			}

			if ((nameValuePairs != null) && (nameValuePairs.size() > 0)) {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			}

			if (img_file != null) {
				/*
				 * ByteArrayOutputStream bos = new ByteArrayOutputStream();
				 * bitmap.compress(CompressFormat.JPEG, 100, bos); byte[] data =
				 * bos.toByteArray();
				 * 
				 * ByteArrayBody bab = new ByteArrayBody(data, fileName+".jpg");
				 * 
				 * MultipartEntity reqEntity = new
				 * MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
				 * reqEntity.addPart("userfile", bab);
				 * httppost.setEntity(reqEntity);
				 */

				builder.addPart(PARAM_file, new FileBody(img_file));
			}

			httppost.setEntity(builder.build());
			// Execute HTTP Post Request
			HttpResponse httpResponse = httpclient.execute(httppost);

			HttpEntity entity = httpResponse.getEntity();
			InputStream is = entity.getContent();

			response = convertStreamToString(is);

			System.out.println();
		} catch (Exception e) {
			System.out.println();
		}

		return response;
	}

	private String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append((line + "\n"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
}