package com.app.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.app.connection.WS_Calls;
import com.app.database.DatabaseManager;
import com.app.objects.Object_Login;
import com.app.objects.Object_Pickup_Request;
import com.app.pickingo.pickboy.Activity_Capture_Signature;
import com.app.pickingo.pickboy.R;
import com.asyncTasks.AsyncReusable;
import com.asyncTasks.Async_RequestDatabase;
import com.asyncTasks.Async_doPickupRequest;
import com.global.Global;
import com.global.ui.RemarksDialog;
import com.global.ui.ui_ProgressDialog;
import com.global.ui.ui_ToastMessage;
import com.viewpagerindicator.TabPageIndicator;

public class Fragment_Home extends Fragment {

	private View parentView;

	// Instances of current class
	FragmentActivity curr_activity;
	Context curr_context;

	Adapter_home_inner_fragments adapter_home_inner_fragments;

	// private static final String[] CONTENT = new String[] { "Request",
	// "Confirm", "Archie"};
	private final String[] CONTENT = new String[] { "PENDING", "ATTEMPTED" };

	public Fragment_Home() {

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		curr_activity = (FragmentActivity) activity;
		curr_context = activity;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestDataBase();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		parentView = inflater.inflate(R.layout.fragment_home, container, false);

		curr_activity = getActivity();
		curr_context = container.getContext();

		init();
		WS_Calls calls = new WS_Calls();

		return parentView;
	}

	private void init() {
		try {
			adapter_home_inner_fragments = new Adapter_home_inner_fragments(getActivity().getSupportFragmentManager());

			ViewPager pager = (ViewPager) parentView.findViewById(R.id.pager);
			pager.setAdapter(adapter_home_inner_fragments);

			TabPageIndicator indicator = (TabPageIndicator) parentView.findViewById(R.id.indicator);

			indicator.setViewPager(pager);
			setHasOptionsMenu(false);
			Object_Login obj_login = Global.getLoginInfo(curr_activity);
			System.out.println();
		} catch (Exception e) {
			System.out.println();
		}

	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		
		super.onPrepareOptionsMenu(menu);
		MenuItem item = menu.findItem(R.id.action_add);
		if (item != null) {
			item.setVisible(false);
		}
		menu.clear();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Add your menu entries here
		super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
		inflater.inflate(R.menu.main, menu);

		// menu.add("Add Images");
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		}

		return false;

	};

	private void requestApi() {
		// making service call if current data is not present in
		// database
		Async_doPickupRequest async_doRequest = new Async_doPickupRequest(curr_activity, curr_context);
		async_doRequest.setOnResultsListener(asyncReusable);
		async_doRequest.execute();
	}

	private void requestDataBase() {
		// checking database if current days data is present
		Async_RequestDatabase async_RequestDatabase = new Async_RequestDatabase(curr_activity, curr_context);
		async_RequestDatabase.setOnResultsListener(asyncReusable);
		async_RequestDatabase.execute();
	}

	AsyncReusable asyncReusable = new AsyncReusable() {

		@Override
		public void onResultsSucceeded(Object result) {
			if (result.toString().contentEquals(AsyncReusable.API_REQUEST_SUCCESS)) {
				requestDataBase();
				return;
			} else if (result.toString().contentEquals(AsyncReusable.API_REQUEST_FAILED)) {
				ui_ToastMessage.getInstance().makeToast(curr_activity, "Some error occured in fetching the data.Check your internet connection");
				ui_ProgressDialog.getInstance().dismiss();
			} else if (result.toString().contentEquals(AsyncReusable.DB_REQUEST_FAILED)) {
				requestApi();
				return;
			} else {

				HashMap<String, ArrayList<Object_Pickup_Request>> map = (HashMap<String, ArrayList<Object_Pickup_Request>>) result;
				Fragment pending_fragment = adapter_home_inner_fragments.getCurrentFragment(0);
				Fragment attempted_fragment = adapter_home_inner_fragments.getCurrentFragment(1);
				((Fragment_Pending) pending_fragment).setPendingList(map.get("PENDING"));
				((Fragment_Attempted) attempted_fragment).setAttemptedList(map.get("ATTEMPTED"));
			}

		}
	};

	class Adapter_home_inner_fragments extends FragmentStatePagerAdapter implements FragmentCallback {
		SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

		public Adapter_home_inner_fragments(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Fragment fragment = null;
			if (position == 0) {
				fragment = new Fragment_Pending(this);
			} else if (position == 1) {
				fragment = new Fragment_Attempted(this);
			}
			registeredFragments.put(position, fragment);
			return fragment;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return CONTENT[position % CONTENT.length].toUpperCase();
		}

		@Override
		public int getCount() {
			return CONTENT.length;
		}

		@Override
		public int getItemPosition(Object object) {
			
			return POSITION_NONE;
		}

		public Fragment getCurrentFragment(int position) {
			return registeredFragments.get(position);
		}

		@Override
		public void refresh() {
			requestDataBase();
		}
	}
}