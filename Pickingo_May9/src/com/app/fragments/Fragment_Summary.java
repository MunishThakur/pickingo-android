package com.app.fragments;

import java.util.HashMap;
import java.util.List;

import com.app.database.DatabaseManager;
import com.app.pickingo.pickboy.R;
import com.global.GlobalAasmStatus;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class Fragment_Summary extends Fragment implements GlobalAasmStatus {

	TextView tv_Pendind, tv_Picked, tv_Postponed, tv_Cancelled, tv_NotContactable;
	Context curr_context;
	Activity curr_activity;

	public Fragment_Summary() {

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		curr_activity = activity;
		curr_context = activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_summary, container, false);
		init(rootView);
		return rootView;
	}

	private void init(View rootView) {
		try {
			tv_Pendind = (TextView) rootView.findViewById(R.id.tv_pending_count);
			tv_Picked = (TextView) rootView.findViewById(R.id.tv_picked_count);
			tv_Postponed = (TextView) rootView.findViewById(R.id.tv_postponed_count);
			tv_Cancelled = (TextView) rootView.findViewById(R.id.tv_cancel_count);
			tv_NotContactable = (TextView) rootView.findViewById(R.id.tv_not_contactable_count);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void setupView(HashMap<String, String> map) {
		try {
			String s_out_pickup_count = map.get(KEY_STATUS_OUT_FOR_PICKUP);
			if (s_out_pickup_count != null && s_out_pickup_count.length() > 0)
				tv_Pendind.setText(s_out_pickup_count);
			String s_pickup_count = map.get(KEY_STATUS_PICKED);
			if (s_pickup_count != null && s_pickup_count.length() > 0)
				tv_Picked.setText(s_pickup_count);
			String s_not_postponed = map.get(KEY_STATUS_CID);
			if (s_not_postponed != null && s_not_postponed.length() > 0)
				tv_Postponed.setText(s_not_postponed);
			String s_cancelled_count = map.get(KEY_STATUS_CANCELLED);
			if (s_cancelled_count != null && s_cancelled_count.length() > 0)
				tv_Cancelled.setText(s_cancelled_count);
			String s_not_contactable = map.get(KEY_STATUS_NOT_CONTACTABLE);
			if (s_not_contactable != null && s_not_contactable.length() > 0)
				tv_NotContactable.setText(s_not_contactable);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			DatabaseManager databaseManager = DatabaseManager.getInstance(getActivity());
			HashMap<String, String> summList = databaseManager.getSummaryData();
			setupView(summList);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
