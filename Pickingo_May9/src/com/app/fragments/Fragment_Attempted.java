package com.app.fragments;

import java.util.List;

import com.app.adapters.Adapter_Attempted_Request;
import com.app.adapters.Adapter_Pending_Request;
import com.app.objects.Object_Pickup_Request;
import com.app.pickingo.pickboy.R;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class Fragment_Attempted extends Fragment {
	FragmentActivity curr_activity;
	Context curr_context;
	FragmentCallback iCallback;
	List<Object_Pickup_Request> mAttemptedList;
	ListView mAttemptedRequestListView;

	public Fragment_Attempted(FragmentCallback callback) {
		iCallback = callback;
	}

	public void setAttemptedList(List<Object_Pickup_Request> attemptedList) {
		mAttemptedList = attemptedList;
		Adapter_Attempted_Request adapter_Request = new Adapter_Attempted_Request(curr_context, mAttemptedList);
		mAttemptedRequestListView.setAdapter(adapter_Request);
	}

	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
		curr_activity = (FragmentActivity) activity;
		curr_context = activity;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_list, container, false);
		mAttemptedRequestListView = (ListView) rootView.findViewById(R.id.lv_pickup);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);

		init();
	}

	private void init() {
		try {
			if (mAttemptedList != null && mAttemptedList.size() > 0) {
				Adapter_Attempted_Request adapter_Request = new Adapter_Attempted_Request(curr_context, mAttemptedList);
				mAttemptedRequestListView.setAdapter(adapter_Request);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
