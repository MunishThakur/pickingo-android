package com.app.fragments;

import java.util.List;

import com.app.adapters.Adapter_Pending_Request;
import com.app.objects.Object_Pickup_Request;
import com.app.pickingo.pickboy.Activity_Request_Details;
import com.app.pickingo.pickboy.R;
import com.global.ui.ui_ProgressDialog;
import com.global.ui.ui_ToastMessage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class Fragment_Pending extends Fragment {
	FragmentActivity curr_activity;
	Context curr_context;

	FragmentCallback mFragmentCallback;
	/*
	 * Singelton Classes
	 */
	ui_ProgressDialog obj_prog = ui_ProgressDialog.getInstance();
	ui_ToastMessage obj_toast = ui_ToastMessage.getInstance();

	List<Object_Pickup_Request> mPendingList;
	ListView mPendingRequestListView;

	public static final int iREQUEST_CODE = 1000;

	public Fragment_Pending(FragmentCallback callback) {
		mFragmentCallback = callback;
	}

	public void setPendingList(List<Object_Pickup_Request> mPendingList) {
		this.mPendingList = mPendingList;
		Adapter_Pending_Request adapter_Request = new Adapter_Pending_Request(curr_context, mPendingList);
		if (mPendingRequestListView != null)
			mPendingRequestListView.setAdapter(adapter_Request);
		obj_prog.dismiss();
	}

	@Override
	public void onAttach(Activity activity) {
		
		super.onAttach(activity);
		curr_activity = (FragmentActivity) activity;
		curr_context = activity;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_list, container, false);
		mPendingRequestListView = (ListView) rootView.findViewById(R.id.lv_pickup);
		mPendingRequestListView.setOnItemClickListener(listItemClickListener);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		
		super.onActivityCreated(savedInstanceState);
		init();
	}
	
	private void init(){
		try {
			obj_prog.makeProgressDialog(curr_activity, curr_context, true);
			obj_prog.prog.setMessage("Please Wait...");
			obj_prog.show();
			if (mPendingList != null) {
				Adapter_Pending_Request adapter_Request = new Adapter_Pending_Request(curr_context, mPendingList);
				if (mPendingRequestListView != null)
					mPendingRequestListView.setAdapter(adapter_Request);
			}
		} catch (Exception e) {
			e.printStackTrace();	
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		super.onActivityResult(requestCode, resultCode, data);
		mFragmentCallback.refresh();
	}

	OnItemClickListener listItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			try {
				Intent intent = new Intent(curr_context, Activity_Request_Details.class);
				Object_Pickup_Request object_Pickup_Request = (Object_Pickup_Request) parent.getItemAtPosition(position);
				intent.putExtra(Activity_Request_Details.KEY_REQUEST_DATA, object_Pickup_Request);
				startActivityForResult(intent, iREQUEST_CODE);
			} catch (Exception e) {
				e.printStackTrace();	
			}
			
		}
	};

}
