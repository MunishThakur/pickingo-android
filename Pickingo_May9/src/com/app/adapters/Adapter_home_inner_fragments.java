package com.app.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;

import com.app.fragments.FragmentCallback;
import com.app.fragments.Fragment_Attempted;
import com.app.fragments.Fragment_Pending;

public class Adapter_home_inner_fragments extends FragmentStatePagerAdapter implements FragmentCallback {
	
	// private static final String[] CONTENT = new String[] { "Request",
		// "Confirm", "Archie"};
		private final String[] CONTENT = new String[] { "PENDING", "ATTEMPTED" };

	SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

	public Adapter_home_inner_fragments(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		Fragment fragment = null;
		if (position == 0) {
			fragment = new Fragment_Pending(this);
		} else if (position == 1) {
			fragment = new Fragment_Attempted(this);
		}
		registeredFragments.put(position, fragment);
		return fragment;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return CONTENT[position % CONTENT.length].toUpperCase();
	}

	@Override
	public int getCount() {
		return CONTENT.length;
	}

	@Override
	public int getItemPosition(Object object) {
		// TODO Auto-generated method stub
		return POSITION_NONE;
	}

	public Fragment getCurrentFragment(int position) {
		return registeredFragments.get(position);
	}

	@Override
	public void refresh() {
		//requestDataBase();
	}
}
