package com.app.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.objects.Object_Pickup_Request;
import com.app.pickingo.pickboy.R;
import com.global.GlobalAasmStatus;

public class Adapter_Attempted_Request extends BaseAdapter implements GlobalAasmStatus {

	Context mContext;
	List<Object_Pickup_Request> mPickRequestList;
	LayoutInflater mLayoutInflater;

	public Adapter_Attempted_Request(Context context, List<Object_Pickup_Request> pickRequestList) {
		super();
		this.mContext = context;
		this.mPickRequestList = pickRequestList;
		mLayoutInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mPickRequestList.size();
	}

	@Override
	public Object getItem(int position) {
		return mPickRequestList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		try {
			if (convertView == null) {
				viewHolder = new ViewHolder();
				convertView = mLayoutInflater.inflate(R.layout.munish_adapter_attempted_request, null);
				viewHolder.addressTextView = (TextView) convertView.findViewById(R.id.tv_address);
				viewHolder.clientContactTextView = (TextView) convertView.findViewById(R.id.tv_client_contact);
				viewHolder.clientNameTextView = (TextView) convertView.findViewById(R.id.tv_client_name);
				viewHolder.aasmStatusTextView = (TextView) convertView.findViewById(R.id.tv_aasm_status);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			Object_Pickup_Request object_Pickup_Request = (Object_Pickup_Request) getItem(position);

			if (object_Pickup_Request != null) {
				final String s_addressLine = object_Pickup_Request.getAddress().getAddress_line();
				final String s_aasmState = object_Pickup_Request.getAasm_state();

				if ((s_aasmState != null)) {
					if (s_aasmState.trim().equalsIgnoreCase(KEY_STATUS_CANCELLED)) {
						viewHolder.aasmStatusTextView.setBackgroundColor(mContext.getResources().getColor(R.color.clr_cancelled));
					} else if (s_aasmState.trim().equalsIgnoreCase(KEY_STATUS_PICKED)) {
						viewHolder.aasmStatusTextView.setBackgroundColor(mContext.getResources().getColor(R.color.clr_picked));
					} else if (s_aasmState.trim().equalsIgnoreCase(KEY_STATUS_CID)) {
						viewHolder.aasmStatusTextView.setBackgroundColor(mContext.getResources().getColor(R.color.clr_postponed));
					} else if (s_aasmState.trim().equalsIgnoreCase(KEY_STATUS_NOT_CONTACTABLE)) {
						viewHolder.aasmStatusTextView.setBackgroundColor(mContext.getResources().getColor(R.color.clr_not_contactable));
					}
				}
				viewHolder.addressTextView.setText(s_addressLine);
				viewHolder.clientNameTextView.setText(object_Pickup_Request.getAddress().getName());
				String contactNumber = object_Pickup_Request.getAddress().getPhone_number();
				viewHolder.clientContactTextView.setText(contactNumber);
				viewHolder.aasmStatusTextView.setText(s_aasmState);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	static class ViewHolder {
		TextView addressTextView;
		TextView clientNameTextView;
		TextView clientContactTextView;
		TextView aasmStatusTextView;
	}

}
