package com.app.adapters;

import java.util.List;

import com.app.objects.Object_Pickup_Request;
import com.app.pickingo.pickboy.R;
import com.gc.materialdesign.views.ButtonRectangle;
import com.global.CallListener;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class Adapter_Pending_Request extends BaseAdapter {
	Context mContext;
	List<Object_Pickup_Request> mPickRequestList;
	LayoutInflater mLayoutInflater;

	public Adapter_Pending_Request(Context context, List<Object_Pickup_Request> pickRequestList) {
		super();
		this.mContext = context;
		this.mPickRequestList = pickRequestList;
		mLayoutInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mPickRequestList.size();
	}

	@Override
	public Object getItem(int position) {
		return mPickRequestList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		try {
			if (convertView == null) {
				viewHolder = new ViewHolder();
				convertView = mLayoutInflater.inflate(R.layout.munish_adapter_pending_pickup, null);
				viewHolder.addressTextView = (TextView) convertView.findViewById(R.id.tv_address);
				viewHolder.clientContactTextView = (TextView) convertView.findViewById(R.id.tv_client_contact);
				viewHolder.clientNameTextView = (TextView) convertView.findViewById(R.id.tv_client_name);
				viewHolder.callButtonRectangle = (ImageButton) convertView.findViewById(R.id.btn_call);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			Object_Pickup_Request object_Pickup_Request = (Object_Pickup_Request) getItem(position);
			viewHolder.addressTextView.setText(object_Pickup_Request.getAddress().getAddress_line());
			viewHolder.clientNameTextView.setText(object_Pickup_Request.getAddress().getName());
			String contactNumber = object_Pickup_Request.getAddress().getPhone_number();
			viewHolder.clientContactTextView.setText(contactNumber);
			viewHolder.callButtonRectangle.setOnClickListener(new CallListener(mContext, contactNumber));
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return convertView;
	}

	static class ViewHolder {
		TextView addressTextView;
		TextView clientNameTextView;
		TextView clientContactTextView;
		ImageButton callButtonRectangle;
	}

}
