package com.app.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper implements DatabaseConstants {
	private String genrateRequestQuery() {
		String query = "Create table " + TABLE_REQUEST + " ( " + COLUMN_ID + " integer primary key autoincrement , " + COLUMN_USER_ID + " text , " + COLUMN_JOB_ID + " integer , "
				+ COLUMN_AASM_STATE + " text , " + COLUMN_REQUEST_DATA + " text , " + COLUMN_DATE_CREATED + " date , " + COLUMN_DATE_UPDATED + " date , " + COLUMN_JOB_DATE_CREATED
				+ " date , " + COLUMN_SCHEDULED_DATE + " date , " + COLUMN_EVENT_OCCURED + " text , " + COLUMN_SYNC_STATUS + " text , " + COLUMN_SIGNATURE + " text  )";
		return query;
	}

	public DatabaseHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(genrateRequestQuery());
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
		if (!db.isReadOnly()) {
			// Enable foreign key constraints
			db.execSQL("PRAGMA foreign_keys=ON;");
		}
	}

}
