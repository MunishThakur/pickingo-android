package com.app.database;

public interface DatabaseConstants {
	int DATABASE_VERSION = 1;
	String DATABASE_NAME = "Pickingo Database";

	String TABLE_REQUEST = "Tab_Request";
	// Columns in Table Tab_Request
	String COLUMN_ID = "_id";
	String COLUMN_USER_ID = "user_id";
	String COLUMN_JOB_ID = "job_id";
	String COLUMN_AASM_STATE = "aasm_state";
	String COLUMN_REQUEST_DATA = "data_request";
	String COLUMN_DATE_CREATED = "date_created";
	String COLUMN_DATE_UPDATED = "date_updated";
	String COLUMN_SCHEDULED_DATE = "scheduled_date";
	String COLUMN_JOB_DATE_CREATED = "job_date_created";
	String COLUMN_EVENT_OCCURED = "event_occured";
	String COLUMN_SYNC_STATUS = "sync_status";
	String COLUMN_SIGNATURE="signature";
	String COLUMN_OPERATION="operation";	
	
	String COLUMN_COUNT="count";
	
	
	
	

	String COLUMN_REQUEST_ID = "_id";

	

}
