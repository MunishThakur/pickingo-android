package com.app.database;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.app.objects.Object_Pickup_Request;
import com.app.objects.Object_Summary;
import com.app.objects.Object_Pickup_Request.Address;
import com.app.objects.Object_Pickup_Request.Skus;
import com.global.Global;
import com.google.gson.Gson;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DatabaseManager implements DatabaseConstants {
	Context mContext;
	SQLiteDatabase mSqLiteDatabase;
	DatabaseHelper mDatabaseHelper;
	static DatabaseManager mDatabaseManager;

	private DatabaseManager(Context context) {
		mContext = context;
		mDatabaseHelper = new DatabaseHelper(mContext, DATABASE_NAME, null, DATABASE_VERSION);
		if (mSqLiteDatabase == null) {
			mSqLiteDatabase = mDatabaseHelper.getWritableDatabase();
		}
	}

	public static DatabaseManager getInstance(Context context) {
		if (mDatabaseManager == null)
			mDatabaseManager = new DatabaseManager(context);
		return mDatabaseManager;
	}

	public static String Object_GsonString(Object_Pickup_Request object) {
		return new Gson().toJson(object);
	}

	public static String Object_GsonString(Object object) {
		return new Gson().toJson(object);
	}

	private String getCurrentDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String formattedDate = dateFormat.format(new Date());
		return formattedDate;
	}

	public long insertPickupRequest(String userId, Object_Pickup_Request object_Pickup_Request) {
		String requestData = Object_GsonString(object_Pickup_Request);
		ContentValues contentValues = new ContentValues();
		try {
			contentValues.put(COLUMN_USER_ID, userId);
			contentValues.put(COLUMN_JOB_ID, object_Pickup_Request.getId());
			contentValues.put(COLUMN_AASM_STATE, object_Pickup_Request.getAasm_state());
			contentValues.put(COLUMN_REQUEST_DATA, requestData);
			contentValues.put(COLUMN_DATE_CREATED, getCurrentDate());
			contentValues.put(COLUMN_SCHEDULED_DATE, object_Pickup_Request.getScheduled_date());
			contentValues.put(COLUMN_JOB_DATE_CREATED, getCurrentDate());
			contentValues.put(COLUMN_EVENT_OCCURED, "false");
			contentValues.put(COLUMN_SYNC_STATUS, "false");
		} catch (Exception e) {
			// TODO: handle exception
		}

		return insertData(TABLE_REQUEST, contentValues);
	}

	public int updatepickupRequest(String userId, Object_Pickup_Request object_Pickup_Request) {
		String updatedData = Object_GsonString(object_Pickup_Request);

		String curr_date = getCurrentDate();
		ContentValues contentValues = new ContentValues();
		try {
			contentValues.put(COLUMN_AASM_STATE, object_Pickup_Request.getAasm_state());
			contentValues.put(COLUMN_REQUEST_DATA, updatedData);
			contentValues.put(COLUMN_DATE_UPDATED, curr_date);
			contentValues.put(COLUMN_EVENT_OCCURED, "true");
			File file = object_Pickup_Request.getSignature();
			if (file != null)
				contentValues.put(COLUMN_SIGNATURE, file.getAbsolutePath());
			contentValues.put(COLUMN_SCHEDULED_DATE, object_Pickup_Request.getScheduled_date());

			String whereClause = COLUMN_USER_ID + " = '" + userId + "' and " + COLUMN_JOB_DATE_CREATED + " = '" + curr_date + "' and " + COLUMN_JOB_ID + " = "
					+ object_Pickup_Request.getId();
			return updateData(TABLE_REQUEST, contentValues, whereClause);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return -1;

	}

	public int updateSyncStatus(boolean status, String userId, Object_Pickup_Request object_Pickup_Request) {
		try {
			String curr_date = getCurrentDate();
			ContentValues contentValues = new ContentValues();
			contentValues.put(COLUMN_SYNC_STATUS, status);
			String whereClause = COLUMN_USER_ID + " = '" + userId + "' and " + COLUMN_JOB_DATE_CREATED + " = '" + curr_date + "' and " + COLUMN_JOB_ID + " = "
					+ object_Pickup_Request.getId();
			return mSqLiteDatabase.update(TABLE_REQUEST, contentValues, whereClause, null);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return -1;

	}

	public ArrayList<Object_Pickup_Request> getPickupRequestData(Cursor cursor) {
		ArrayList<Object_Pickup_Request> mPickupList = new ArrayList<>();
		try {
			if (cursor != null && cursor.getCount() > 0) {
				cursor.moveToFirst();
				do {
					String data = cursor.getString(cursor.getColumnIndex(COLUMN_REQUEST_DATA));
					String signature = cursor.getString(cursor.getColumnIndex(COLUMN_SIGNATURE));
					Object_Pickup_Request object_Pickup_Request = new Gson().fromJson(data, Object_Pickup_Request.class);
					if (signature != null && signature.length() > 0)
						object_Pickup_Request.setSignature(new File(signature));
					mPickupList.add(object_Pickup_Request);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mPickupList;
	}

	public Cursor getTodaysJob(String userId) {
		String whereClause = COLUMN_USER_ID + " = '" + userId + "' and " + COLUMN_JOB_DATE_CREATED + " = '" + getCurrentDate() + "'";
		return queryAllData(whereClause);
	}

	public HashMap<String, String> getSummaryData() {
		String query = "SELECT " + COLUMN_AASM_STATE + " ,  COUNT(*) AS " + COLUMN_COUNT + " FROM " + TABLE_REQUEST + " Where " + COLUMN_JOB_DATE_CREATED + " = '"
				+ getCurrentDate() + "'" + " GROUP BY " + COLUMN_AASM_STATE;

		Cursor cursor = mSqLiteDatabase.rawQuery(query, null);
		HashMap<String, String> map = new HashMap<>();
		if (cursor.moveToFirst()) {
			do {
				String s_aasm_state = cursor.getString(cursor.getColumnIndex(COLUMN_AASM_STATE));
				String s_value = Integer.toString(cursor.getInt(cursor.getColumnIndex(COLUMN_COUNT)));
				map.put(s_aasm_state, s_value);
			} while (cursor.moveToNext());

		}
		return map;
	}

	public int deletePreviousData() {
		String whereClause = COLUMN_JOB_DATE_CREATED + " <> '" + getCurrentDate() + "'";
		return deleteData(TABLE_REQUEST, null, whereClause);
	}

	public ArrayList<Object_Pickup_Request> getDataForSync() {
		String selection = COLUMN_SYNC_STATUS + " = 'false' and " + COLUMN_EVENT_OCCURED + " = 'true' and " + COLUMN_JOB_DATE_CREATED + " = '" + getCurrentDate() + "'";
		return getPickupRequestData(queryAllData(selection));
		// Cursor cursor = mSqLiteDatabase.query(TABLE_REQUEST, null, null,
		// null, null, null, null);
		// return getPickupRequestData(cursor);
	}

	private void updateRequest(Object_Pickup_Request updated_Object_Pickup_Request) {

	}

	/****************************************************/
	private long insertData(String tableName, ContentValues values) {
		return mSqLiteDatabase.insert(tableName, null, values);
	}

	private int updateData(String tableName, ContentValues values, String whereClause) {
		return mSqLiteDatabase.update(tableName, values, whereClause, null);
	}

	private Cursor queryAllData(String whereClause) {
		String queryString = "select * from " + TABLE_REQUEST + " where " + whereClause;
		Cursor cursor = mSqLiteDatabase.rawQuery(queryString, null);
		return cursor;
	}

	private int deleteData(String tableName, ContentValues values, String whereClause) {
		return mSqLiteDatabase.delete(tableName, whereClause, null);
	}
}
