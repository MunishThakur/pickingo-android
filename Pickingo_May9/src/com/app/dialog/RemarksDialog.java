package com.app.dialog;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.Gravity;
import android.widget.EditText;

import com.global.ui.ui_ToastMessage;
import com.throrinstudio.android.common.libs.validator.Form;
import com.throrinstudio.android.common.libs.validator.Validate;
import com.throrinstudio.android.common.libs.validator.validator.NotEmptyValidator;

public class RemarksDialog extends Builder {
	Activity curr_activity = null;
	Context curr_context;
	onRemarksSubmittedListener mListener;
	ui_ToastMessage obj_toast = ui_ToastMessage.getInstance();
	
	public RemarksDialog(Context context, onRemarksSubmittedListener listener) {
		super(context);
		
		curr_activity = (Activity) context;
		curr_context = context;
		mListener = listener;
	}

	Form mForm = new Form();

	private void addValidator_NonEmpty(EditText editText) {

		Validate valid_remarks = new Validate(editText);
		valid_remarks.addValidator(new NotEmptyValidator(curr_context));
		mForm.addValidates(valid_remarks);

	}

	public void showDialog() {
		onCreateDialog();
	}

	public void onCreateDialog() {
		setTitle("Enter Remarks");
		final EditText editText = new EditText(curr_context);
		editText.setMinLines(6);

		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.setMargins(5, 5, 5, 5);
		editText.setLayoutParams(params);
		editText.setHint("Enter your remarks here");
		addValidator_NonEmpty(editText);
		editText.setGravity(Gravity.TOP);
		editText.setPadding(15, 15, 15, 15);


		setView(editText);
		setPositiveButton("Submit", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				//if (mForm.validate()) {
				String val = editText.getText().toString();
				if(val != null && (val.trim().length() > 0)){
					mListener.onRemarksRecieved(val);
					dialog.dismiss();
				} else {
					obj_toast.makeToast(curr_activity, "Please fill text");	
				}
				
				//}
			}
		});

		setNegativeButton("Cancel", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		show();
	}

	public interface onRemarksSubmittedListener {
		void onRemarksRecieved(String remarks);
	}
}
