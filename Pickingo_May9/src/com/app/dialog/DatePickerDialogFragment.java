package com.app.dialog;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.DatePicker;
import android.widget.TimePicker;

public class DatePickerDialogFragment extends DialogFragment {
	OnDateSetListener mDateSetListener;

	public DatePickerDialogFragment(OnDateSetListener dateSetListener) {
		mDateSetListener = dateSetListener;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		try {
			// Use the current date as the default date in the picker
			long millis = System.currentTimeMillis();
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			// Create a new instance of DatePickerDialog and return it
			DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), mDateSetListener, year, month, day);
//			datePickerDialog.setTitle(day + "/" + month + "/" + year);
			datePickerDialog.getDatePicker().setMinDate(millis);
			return datePickerDialog;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

}