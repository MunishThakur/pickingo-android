package com.app.pickingo.pickboy;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.app.custom_android_components.Custom_ActionBarActivity_With_Back;
import com.gc.materialdesign.widgets.Dialog;

public class Activity_Pick_Procedure extends Custom_ActionBarActivity_With_Back {
	Dialog mDialog;
	private static final int iSCANNER_REQUEST_CODE = 0;
	Context curr_context = this;
	Activity curr_Activity = this;
	String mBarcodeString = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		startScanner();
	}

	private void startScanner() {
		try {
			Intent intent = new Intent("com.google.zxing.client.android.SCAN");
			intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
			startActivityForResult(intent, iSCANNER_REQUEST_CODE);

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, intent);
		try {
			if (requestCode == iSCANNER_REQUEST_CODE) {
				if (resultCode == RESULT_OK) {
					mBarcodeString = intent.getStringExtra("SCAN_RESULT");
					String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
					String message = String.format(getString(R.string.str_dlg_msg_barcode_scan_success), mBarcodeString);
					showDialog(getString(R.string.app_name), message);
					// Handle successful scan
				} else if (resultCode == RESULT_CANCELED) {
					// Handle cancel
					showDialogWithEditText(getString(R.string.str_dlg_ttl_enter_barcode), getString(R.string.str_dlg_msg_barcode_enter_manual));
				}
			}	
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	private void setResult() {
		setResult(Activity.RESULT_OK, new Intent().putExtra("barcode", mBarcodeString));
		finish();
	}

	private void showDialog(final String title, final String message) {
		Builder dialogBuilder = new Builder(this);
		dialogBuilder.setTitle(title);
		dialogBuilder.setMessage(message);
		dialogBuilder.setPositiveButton(getString(R.string.str_dlg_ok), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				setResult();
			}
		});
		dialogBuilder.setNegativeButton(getString(R.string.str_dlg_cancel), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				showDialogWithEditText(getString(R.string.str_dlg_ttl_enter_barcode), getString(R.string.str_dlg_msg_barcode_enter_manual));
			}
		});
		dialogBuilder.show();
	}

	private void showDialogWithEditText(String title, String message) {
		Builder dialogBuilder = new Builder(this);
		dialogBuilder.setTitle(title);
		dialogBuilder.setMessage(message);
		final EditText editText = new EditText(curr_context);
		LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		editText.setLayoutParams(layoutParams);
		dialogBuilder.setView(editText);

		dialogBuilder.setPositiveButton(getString(R.string.str_dlg_ok), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				mBarcodeString = editText.getText().toString();
				setResult();

			}
		});
		dialogBuilder.setNegativeButton(getString(R.string.str_dlg_cancel), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				setResult(Activity.RESULT_CANCELED);
				dialog.dismiss();
				finish();
			}
		});
		dialogBuilder.show();
	}

}
