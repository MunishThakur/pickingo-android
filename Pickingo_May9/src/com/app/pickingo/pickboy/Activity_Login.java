package com.app.pickingo.pickboy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.app.objects.Object_Login;
import com.asyncTasks.AsyncReusable;
import com.asyncTasks.Async_doLogin;
import com.gc.materialdesign.views.ButtonRectangle;
import com.global.Global;
import com.global.sharedPreferences.SharedPreferences_Bean;
import com.global.sharedPreferences.SharedPreferences_custom;
import com.global.ui.ui_ToastMessage;
import com.google.gson.Gson;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.throrinstudio.android.common.libs.validator.Form;
import com.throrinstudio.android.common.libs.validator.Validate;
import com.throrinstudio.android.common.libs.validator.validator.EmailValidator;
import com.throrinstudio.android.common.libs.validator.validator.NotEmptyValidator;

public class Activity_Login extends Activity implements OnClickListener{

	private Activity curr_activity = this;
	private Context curr_context = this;

	/*
	 * Shared Preferences
	 */
	SharedPreferences_custom obj_sp_login = new SharedPreferences_custom(curr_activity, SharedPreferences_Bean.KEY_LOGIN_DETAILS);

	/*
	 * Singelton Classes
	 */
	ui_ToastMessage obj_toast = ui_ToastMessage.getInstance();

	/*
	 * Form Validator
	 */
	Form mForm = new Form();

	/*
	 * Views
	 */
	MaterialEditText et_login_username;
	MaterialEditText et_login_password;

	ButtonRectangle btn_login;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);
		if(Global.isAutologin(curr_activity)){
			login_In_App();
		} else{
			init();
		}
	}

	private void init(){
		try{
			et_login_username = (MaterialEditText) findViewById(R.id.et_login_username);
			et_login_password = (MaterialEditText) findViewById(R.id.et_login_password);

			// Temp Code
			{
				et_login_username.setText("9953204191");
				et_login_password.setText("pickingo");
			}

			btn_login = (ButtonRectangle) findViewById(R.id.btn_login);
			btn_login.setOnClickListener(this);

			addValidator_Login();
		} catch(Exception e){

		}
	}

	private void addValidator_Login(){

		Validate valid_user_id = new Validate(et_login_username);
		valid_user_id.addValidator(new NotEmptyValidator(curr_context));
		//valid_user_id.addValidator(new EmailValidator(curr_context));

		Validate valid_user_password = new Validate(et_login_password);
		valid_user_password.addValidator(new NotEmptyValidator(curr_context));

		mForm.addValidates(valid_user_id);
		mForm.addValidates(valid_user_password);

	}

	private boolean SaveLoginCredentials(final String resultSoap){
		try {
			obj_sp_login.putString(SharedPreferences_Bean.KEY_LOGIN_DETAILS, resultSoap);

			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	private void login_In_App(){
		try{
			Intent intent = new Intent(curr_context, Activity_Main.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			startActivity(intent);
			finish();
		} catch(Exception e){

		}
	}

	private void init_login(){
		try{

			if(mForm.validate()){
				String s_user_name = et_login_username.getText().toString().trim();
				String s_user_password = et_login_password.getText().toString().trim();

				Async_doLogin obj_async = new Async_doLogin(curr_activity, curr_context, s_user_name, s_user_password);
				obj_async.setOnResultsListener(new AsyncReusable() {

					@Override
					public void onResultsSucceeded(Object result) {

						try{
							Object_Login obj_result = (Object_Login) result;
							if((obj_result != null) && (obj_result.getData() != null) && (obj_result.getData().getUid().length() > 0) && (obj_result.getAl_errors().size() <= 0)){

								String result_json = new Gson().toJson(obj_result);
								SaveLoginCredentials(result_json);
								obj_toast.makeToast(curr_activity, "Logged In");
								login_In_App();
							} else if((obj_result.getData() != null) && (obj_result.getAl_errors().size() > 0)){
								obj_toast.makeToast(curr_activity,  obj_result.getAl_errors().get(0));
							}
							else{
								obj_toast.makeToast(curr_activity, "Error in Service..");
							}
						} catch(Exception e){

						}
					}
				});
				obj_async.execute();
			}
		} catch(Exception e){

		}
	}
	
	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch(id){
		case R.id.btn_login:
		{
			init_login();
			break;
		}
		}
	}

}
