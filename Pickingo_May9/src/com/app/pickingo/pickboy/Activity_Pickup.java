package com.app.pickingo.pickboy;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.app.custom_android_components.Custom_ActionBarActivity_With_Back;
import com.app.database.DatabaseManager;
import com.app.objects.Object_Pickup_Request;
import com.asyncTasks.AsyncReusable;
import com.asyncTasks.Async_doUpdateRequest;
import com.gc.materialdesign.views.ButtonRectangle;
import com.global.Global;
import com.global.GlobalAasmStatus;
import com.global.UpdateProc;
import com.global.ui.ui_ToastMessage;
import com.global.view.ExpandableHeightListView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Activity_Pickup extends Custom_ActionBarActivity_With_Back implements GlobalAasmStatus, OnClickListener {
	private Activity curr_activity = this;
	private Context curr_context = this;

	private static final int iREQCODE_BARCODE = 1;
	private static final int iREQCODE_SIGNATURE = 2;

	Object_Pickup_Request objCurrentRequest;

	TextView mClientNameTextView, mClientAddTextView, mClientOrderNumberTextView, mClientContactNumberTextView, tv_pickup_barcode_val;
	ButtonRectangle mSubmitButtonRectangle, btn_barcode, btn_signature;
	EditText mRemarksEditText;
	ImageView iv_signature;

	ui_ToastMessage obj_toast = ui_ToastMessage.getInstance();

	private static String currentProcess = KEY_STATUS_PICKED;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pickup);
		init();
	}

	private void init() {
		try {
			mClientContactNumberTextView = (TextView) findViewById(R.id.tv_client_contact_no);
			mClientAddTextView = (TextView) findViewById(R.id.tv_client_address);
			mClientOrderNumberTextView = (TextView) findViewById(R.id.tv_client_order_no);
			mClientNameTextView = (TextView) findViewById(R.id.tv_client_name);
			tv_pickup_barcode_val = (TextView) findViewById(R.id.tv_pickup_barcode_val);

			iv_signature = (ImageView) findViewById(R.id.iv_signature);

			mRemarksEditText = (EditText) findViewById(R.id.et_remarks);
			mSubmitButtonRectangle = (ButtonRectangle) findViewById(R.id.btn_submit);
			btn_barcode = (ButtonRectangle) findViewById(R.id.btn_barcode);
			btn_signature = (ButtonRectangle) findViewById(R.id.btn_signature);

			objCurrentRequest = (Object_Pickup_Request) getIntent().getExtras().getSerializable(Activity_Request_Details.KEY_REQUEST_DATA);

			setUpData(objCurrentRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void setUpData(Object_Pickup_Request object_Pickup_Request) {
		try {
			mClientNameTextView.setText(object_Pickup_Request.getAddress().getName());
			mClientOrderNumberTextView.setText(object_Pickup_Request.getClient_order_number());
			mClientAddTextView.setText(object_Pickup_Request.getAddress().getAddress_line());
			mClientContactNumberTextView.setText(object_Pickup_Request.getAddress().getPhone_number());

			mSubmitButtonRectangle.setOnClickListener(this);
			btn_barcode.setOnClickListener(this);
			btn_signature.setOnClickListener(this);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.activity__pickup, menu);
		return true;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == iREQCODE_BARCODE) {
			resultBarcode(resultCode, intent);
		} else if (requestCode == iREQCODE_SIGNATURE) {
			resultSignature(resultCode, intent);
			// put image in the
			// Put data in Database and then Sync data and update the sync
			// status in data base

		}

	}

	private void initRequest() {
		List<NameValuePair> valuePairs = new ArrayList<NameValuePair>();

		valuePairs.add(new BasicNameValuePair("id", objCurrentRequest.getClient_request_id()));
		valuePairs.add(new BasicNameValuePair("remarks", objCurrentRequest.getRemarks()));
		valuePairs.add(new BasicNameValuePair("length", objCurrentRequest.length));
		valuePairs.add(new BasicNameValuePair("height", objCurrentRequest.height));
		valuePairs.add(new BasicNameValuePair("breadth", objCurrentRequest.breadth));
		valuePairs.add(new BasicNameValuePair("weight", objCurrentRequest.weight));
		valuePairs.add(new BasicNameValuePair("aasm_state", objCurrentRequest.aasm_state));
		valuePairs.add(new BasicNameValuePair("scheduled_date", objCurrentRequest.scheduled_date));
		valuePairs.add(new BasicNameValuePair("sku_attributes", objCurrentRequest.getSkus().toString()));
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.btn_barcode: {
			barcodeSubroutine();
			break;
		}
		case R.id.btn_signature: {
			signatureSubroutine();
			break;
		}
		case R.id.btn_submit: {
			// adding the remarks to the object here
			objCurrentRequest.setAasm_state(KEY_STATUS_PICKED);
			objCurrentRequest.setRemarks(mRemarksEditText.getText().toString());
			submitSubroutine();
			break;
		}
		default:
			break;
		}
	}

	private void barcodeSubroutine() {
		try {
			Intent intent = new Intent(curr_context, Activity_Pick_Procedure.class);
			startActivityForResult(intent, iREQCODE_BARCODE);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void signatureSubroutine() {
		try {
			Intent intent = new Intent(this, Activity_Capture_Signature.class);
			intent.putExtra("filename", objCurrentRequest.getAddress().getName() + objCurrentRequest.getId() + ".png");
			startActivityForResult(intent, iREQCODE_SIGNATURE);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void resultBarcode(int resultCode, Intent intent) {
		try {
			if (resultCode == RESULT_OK) {
				String barcode = intent.getStringExtra("barcode");
				objCurrentRequest.setBarcode(barcode);
				tv_pickup_barcode_val.setText(barcode);
			} else if (resultCode == RESULT_CANCELED) {
				objCurrentRequest.setBarcode("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void resultSignature(int resultCode, Intent intent) {
		try {
			if (resultCode == RESULT_OK) {
				File signature = (File) intent.getSerializableExtra("sign");
				objCurrentRequest.setSignature(signature);
				showSignature(signature);
			} else if (resultCode == RESULT_CANCELED) {
				objCurrentRequest.setSignature(null);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void showSignature(File file) {
		try {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
			iv_signature.setImageBitmap(bitmap);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void submitSubroutine() {
		try {
			String barcode = objCurrentRequest.getBarcode();
			String remarks = objCurrentRequest.getRemarks();
			File signature = objCurrentRequest.getSignature();
			if (barcode == null || barcode.trim().length() <= 0) {
				obj_toast.makeToast(curr_activity, getString(R.string.str_tst_barcode_required));
				return;
			} else if (signature == null) {
				obj_toast.makeToast(curr_activity, getString(R.string.str_tst_signature_requred));
				return;
			} else if (remarks == null || remarks.trim().length() <= 0) {
				obj_toast.makeToast(curr_activity, getString(R.string.str_txt_hint_remarks));
				return;
			} else {
				UpdateProc.initUpdateProcess(curr_activity, curr_context, objCurrentRequest);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
