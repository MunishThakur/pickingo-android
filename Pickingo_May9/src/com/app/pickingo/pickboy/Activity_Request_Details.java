package com.app.pickingo.pickboy;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.custom_android_components.Custom_ActionBarActivity_With_Back;
import com.app.dialog.RemarksDialog;
import com.app.objects.Object_Pickup_Request;
import com.app.objects.Object_Pickup_Request.Skus;
import com.gc.materialdesign.views.ButtonRectangle;
import com.gc.materialdesign.views.CheckBox;
import com.gc.materialdesign.views.CheckBox.OnCheckListener;
import com.global.CallListener;
import com.global.GlobalAasmStatus;
import com.global.UpdateProc;
import com.global.ui.ui_ToastMessage;
import com.global.view.ExpandableHeightListView;

public class Activity_Request_Details extends Custom_ActionBarActivity_With_Back implements OnClickListener, GlobalAasmStatus {
	public static final String KEY_REQUEST_DATA = "request_data";
	public static final int iREQUEST_CODE = 1001;
	public static final int iRESULT_CODE2 = 1002;
	Object_Pickup_Request currentRequest;
	int iITEM_SELECTED_COUNT = 0, iMaxSkuSize = 0;

	private Activity curr_activity = this;
	private Context curr_context = this;
	ui_ToastMessage obj_Toast = ui_ToastMessage.getInstance();
	// private static final String CC_Number = curr"999999999";

	TextView mClientNameTextView, mClientAddTextView, mClientOrderNumberTextView, mLocalityTextView, mSkuItemCountTextView, mCallCustomerTextView, mCallSupportTextView,
			mCustomerSupportContactNumberTextView;
	ExpandableHeightListView mSkusListView;
	ButtonRectangle mPickButton, mCancelButton, mNotContactableButton, mPostponeButton;
	Adapter_Skus mAdapter_Skus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_request_details);
		init();
	}

	private void init() {
		try {
			mCustomerSupportContactNumberTextView = (TextView) findViewById(R.id.tv_client_contact);
			mClientAddTextView = (TextView) findViewById(R.id.tv_client_address);
			mClientOrderNumberTextView = (TextView) findViewById(R.id.tv_client_order_number);
			mLocalityTextView = (TextView) findViewById(R.id.tv_client_locality);
			mSkuItemCountTextView = (TextView) findViewById(R.id.tv_items_count);
			mClientNameTextView = (TextView) findViewById(R.id.tv_client_name);

			mSkusListView = (ExpandableHeightListView) findViewById(R.id.lv_skus);
			mSkusListView.setExpanded(true);

			mCallCustomerTextView = (TextView) findViewById(R.id.tv_call_customer);
			mCallSupportTextView = (TextView) findViewById(R.id.tv_call_picking_support);
			mPickButton = (ButtonRectangle) findViewById(R.id.btn_pick);
			mCancelButton = (ButtonRectangle) findViewById(R.id.btn_cancel);
			mNotContactableButton = (ButtonRectangle) findViewById(R.id.btn_not_contactable);
			mPostponeButton = (ButtonRectangle) findViewById(R.id.btn_postpone);

			mPickButton.setOnClickListener(this);
			mCancelButton.setOnClickListener(this);
			mNotContactableButton.setOnClickListener(this);
			mPostponeButton.setOnClickListener(this);
			currentRequest = (Object_Pickup_Request) getIntent().getExtras().getSerializable(KEY_REQUEST_DATA);

			// buttons for calling
			mCallSupportTextView.setOnClickListener(new CallListener(curr_context, getString(R.string.str_customer_support_number)));
			mCallCustomerTextView.setOnClickListener(new CallListener(curr_context, currentRequest.getAddress().getPhone_number()));

			setUpData(currentRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void setUpData(Object_Pickup_Request object_Pickup_Request) {
		try {
			mClientNameTextView.setText(object_Pickup_Request.getAddress().getName());
			mClientOrderNumberTextView.setText(object_Pickup_Request.getClient_order_number());
			mClientAddTextView.setText(object_Pickup_Request.getAddress().getAddress_line());
			iMaxSkuSize = object_Pickup_Request.getSkus().size();
			mSkuItemCountTextView.setText(String.format(getString(R.string.str_items_selectd), iITEM_SELECTED_COUNT, iMaxSkuSize));

			String locality = object_Pickup_Request.getAddress().getLocality();
			if (locality == null || locality.length() <= 0) {
				mLocalityTextView.setVisibility(View.GONE);
			} else {
				mLocalityTextView.setVisibility(View.VISIBLE);
			}
			mLocalityTextView.setText(locality);

			mAdapter_Skus = new Adapter_Skus(object_Pickup_Request.getSkus());
			mSkusListView.setAdapter(mAdapter_Skus);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_pick:
			pickProc();
			break;
		case R.id.btn_cancel:
			nonPickProc(KEY_STATUS_CANCELLED);
			break;
		case R.id.btn_not_contactable:
			nonPickProc(KEY_STATUS_NOT_CONTACTABLE);
			break;
		case R.id.btn_postpone:
			postponeProc();
			break;
		default:
			break;
		}
	}

	private void pickProc() {
		try {
			if (iITEM_SELECTED_COUNT > 0) {
				Intent intent = new Intent(curr_context, Activity_Pickup.class);
				intent.putExtra(KEY_REQUEST_DATA, currentRequest);
				startActivityForResult(intent, iREQUEST_CODE);
			} else {
				obj_Toast.makeToast(curr_activity, getString(R.string.str_tst_item_selection));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		// TODO Auto-generated method stub
		super.onActivityResult(arg0, arg1, arg2);
		if (arg0 == iREQUEST_CODE && arg1 == RESULT_CODE_DONE) {
			finish();
		}
	}

	private void nonPickProc(final String aasm_status) {
		try {
			currentRequest.setAasm_state(aasm_status);
			new RemarksDialog(this, new RemarksDialog.onRemarksSubmittedListener() {

				@Override
				public void onRemarksRecieved(String remarks) {
					// submit api request perform action here
					currentRequest.setRemarks(remarks);
					UpdateProc.initUpdateProcess(curr_activity, curr_context, currentRequest);
				}
			}).showDialog();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void postponeProc() {
		try {
			Intent intent = new Intent(curr_context, Activity_Postpone.class);
			intent.putExtra(KEY_REQUEST_DATA, currentRequest);
			startActivityForResult(intent, iREQUEST_CODE);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public class Adapter_Skus extends BaseAdapter {
		List<Skus> mSkusList;
		LayoutInflater mLayoutInflater;
		OnClickListener mClickListener;

		public Adapter_Skus(List<Skus> list) {
			mSkusList = list;
			mLayoutInflater = (LayoutInflater) curr_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		}

		@Override
		public int getCount() {
			return mSkusList.size();
		}

		@Override
		public Object getItem(int position) {
			return mSkusList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			try {
				if (convertView == null)
					convertView = mLayoutInflater.inflate(R.layout.adapter_sku, null);
				final CheckBox box = (CheckBox) convertView.findViewById(R.id.cb_select_sku);
				final TextView skuName = (TextView) convertView.findViewById(R.id.tv_sku_name);
				final Skus skus = (Skus) getItem(position);

				skuName.setText(skus.getName());

				box.setOncheckListener(new OnCheckListener() {

					@Override
					public void onCheck(boolean isChecked) {
						skus.selected = isChecked;
						// skus.setPicked(isChecked);
						if (isChecked)
							iITEM_SELECTED_COUNT++;
						else
							iITEM_SELECTED_COUNT--;

						mSkuItemCountTextView.setText(String.format(getString(R.string.str_items_selectd), iITEM_SELECTED_COUNT, iMaxSkuSize));

					}
				});

				convertView.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						boolean isChecked = skus.selected;

						// skus.setPicked(isChecked);
						if (isChecked) {
							iITEM_SELECTED_COUNT--;
							skus.selected = false;
							box.setChecked(false);
						} else {
							iITEM_SELECTED_COUNT++;
							skus.selected = true;
							box.setChecked(true);
						}

						mSkuItemCountTextView.setText(String.format(getString(R.string.str_items_selectd), iITEM_SELECTED_COUNT, iMaxSkuSize));
					}
				});

			} catch (Exception e) {
				e.printStackTrace();
			}

			return convertView;
		}

	}

}
