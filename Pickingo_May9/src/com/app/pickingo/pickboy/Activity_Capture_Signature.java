package com.app.pickingo.pickboy;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.app.custom_android_components.Custom_ActionBarActivity_With_Back;
import com.global.view.SignatureMainLayout;

public class Activity_Capture_Signature extends Custom_ActionBarActivity_With_Back {
	Context curr_context = this;
	Activity curr_Activity = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		init();
	}

	private void init() {
		try {
			String fileName = getIntent().getExtras().getString("filename");
			setContentView(new SignatureMainLayout(curr_Activity, fileName));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED);
		super.onBackPressed();
	}
}
