package com.app.pickingo.pickboy;

import java.text.DateFormatSymbols;

import android.app.Activity;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.app.custom_android_components.Custom_ActionBarActivity_With_Back;
import com.app.dialog.DatePickerDialogFragment;
import com.app.objects.Object_Pickup_Request;
import com.gc.materialdesign.views.ButtonRectangle;
import com.global.GlobalAasmStatus;
import com.global.UpdateProc;
import com.global.ui.ui_ToastMessage;

public class Activity_Postpone extends Custom_ActionBarActivity_With_Back implements GlobalAasmStatus {
	Object_Pickup_Request currentRequest;
	int iITEM_SELECTED_COUNT = 0, iMaxSkuSize = 0;

	private Activity curr_activity = this;
	private Context curr_context = this;
	ui_ToastMessage obj_Toast = ui_ToastMessage.getInstance();

	TextView mClientNameTextView, mClientAddTextView, mClientOrderNumberTextView, mLocalityTextView, mDateTextView;
	EditText mRemarksEditText;
	ButtonRectangle mSubmitButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_postpone);
		init();
	}

	private void init() {
		try {
			mClientAddTextView = (TextView) findViewById(R.id.tv_client_address);
			mClientOrderNumberTextView = (TextView) findViewById(R.id.tv_client_order_number);
			mLocalityTextView = (TextView) findViewById(R.id.tv_client_locality);
			mClientNameTextView = (TextView) findViewById(R.id.tv_client_name);
			mDateTextView = (TextView) findViewById(R.id.tv_date);

			mRemarksEditText = (EditText) findViewById(R.id.et_remarks);

			mSubmitButton = (ButtonRectangle) findViewById(R.id.btn_submit);

			currentRequest = (Object_Pickup_Request) getIntent().getExtras().getSerializable(Activity_Request_Details.KEY_REQUEST_DATA);
			setUpData(currentRequest);
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	private void setUpData(Object_Pickup_Request object_Pickup_Request) {
		try {
			mClientNameTextView.setText(object_Pickup_Request.getAddress().getName());
			mClientOrderNumberTextView.setText(object_Pickup_Request.getClient_order_number());
			mClientAddTextView.setText(object_Pickup_Request.getAddress().getAddress_line());
			mLocalityTextView.setText(object_Pickup_Request.getAddress().getPhone_number());
			mDateTextView.setText(object_Pickup_Request.getDate_created());

			mDateTextView.setOnClickListener(clickListener);
			mSubmitButton.setOnClickListener(clickListener);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.tv_date:
				showDatePickerDialog();
				break;
			case R.id.btn_submit:
				submit();
				break;

			default:
				break;
			}

		}
	};

	public void showDatePickerDialog() {
		DialogFragment newFragment = new DatePickerDialogFragment(new OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				try {
					monthOfYear++;
					mDateTextView.setText(appendZero(dayOfMonth) + "/" + appendZero(monthOfYear) + "/" + year);
					// changing the date in mm/dd/yyyy format as asked in the
					// mail
					String scheduledDate = appendZero(monthOfYear) + "/" + appendZero(dayOfMonth) + "/" + year;
					currentRequest.setScheduled_date(scheduledDate);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		newFragment.show(getFragmentManager(), "timePicker");
	}

	private void submit() {
		try {
			currentRequest.setAasm_state(KEY_STATUS_CID);
			String remarks = mRemarksEditText.getText().toString();
			// checking if remarks is empty for validation
			if (remarks != null && remarks.trim().length() > 0) {
				currentRequest.setRemarks(remarks);
				UpdateProc.initUpdateProcess(curr_activity, curr_context, currentRequest);
			} else {
				obj_Toast.makeToast(curr_activity, "Please enter remarks");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String getMonth(int month) {
		return new DateFormatSymbols().getMonths()[month - 1];
	}

	public static String appendZero(int value) {
		if (value <= 9)
			return "0" + value;
		return Integer.toString(value);
	}
}
