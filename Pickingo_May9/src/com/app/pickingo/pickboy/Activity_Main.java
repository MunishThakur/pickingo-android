package com.app.pickingo.pickboy;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.app.adapters.Adapter_NavDrawer;
import com.app.database.DatabaseManager;
import com.app.fragments.Fragment_Home;
import com.app.fragments.Fragment_Summary;
import com.app.objects.Object_NavDrawerItem;
import com.app.objects.Object_Pickup_Request;
import com.asyncTasks.AsyncReusable;
import com.asyncTasks.Async_doSync;
import com.global.Global;
import com.global.ui.ui_ToastMessage;

@SuppressLint("NewApi")
public class Activity_Main extends FragmentActivity {

	private final Activity curr_activity = this;
	private final Context curr_context = this;

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<Object_NavDrawerItem> navDrawerItems;
	private Adapter_NavDrawer adapter;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mTitle = mDrawerTitle = getTitle();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		populateNavDrawer();

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0);
		}
		// removing previous database entry
		cleanPreviousEntry();
	}

	private void cleanPreviousEntry() {

		DatabaseManager databaseManager = DatabaseManager.getInstance(curr_context);
		 databaseManager.deletePreviousData();

	}

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action bar actions click
		switch (item.getItemId()) {

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		// menu.findItem(R.id.action_contact_us).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	private void displayView(int position) {
		// update the main content by replacing fragmentssssss
		Fragment fragment = null;
		switch (position) {
		case 0:
			fragment = new Fragment_Home();
			break;
		case 1:
			fragment = new Fragment_Summary();
			break;
		case 2:
			syncData();
			break;
		case 3:
			logout();
			break;
		default:
			break;
		}

		if (fragment != null) {
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			if (navMenuTitles.length >= position)
				setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	private void populateNavDrawer() {
		try {

			// load slide menu items
			navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

			// nav drawer icons from resources
			navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

			navDrawerItems = new ArrayList<Object_NavDrawerItem>();

			// adding nav drawer items to array
			// Home
			navDrawerItems.add(new Object_NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
			navDrawerItems.add(new Object_NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
			navDrawerItems.add(new Object_NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
			navDrawerItems.add(new Object_NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));

			// Recycle the typed array
			navMenuIcons.recycle();

			mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

			// setting the nav drawer list adapter
			adapter = new Adapter_NavDrawer(getApplicationContext(), navDrawerItems);
			mDrawerList.setAdapter(adapter);

			// enabling action bar app icon and behaving it as toggle button
			getActionBar().setDisplayHomeAsUpEnabled(true);
			getActionBar().setHomeButtonEnabled(true);

			mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, // nav
																									// menu
																									// toggle
																									// icon
					R.string.app_name, // nav drawer open - description for
										// accessibility
					R.string.app_name // nav drawer close - description for
										// accessibility
			) {
				public void onDrawerClosed(View view) {
					getActionBar().setTitle(mTitle);
					// calling onPrepareOptionsMenu() to show action bar icons
					invalidateOptionsMenu();
				}

				public void onDrawerOpened(View drawerView) {
					getActionBar().setTitle(mDrawerTitle);
					// calling onPrepareOptionsMenu() to hide action bar icons
					invalidateOptionsMenu();
				}
			};
			mDrawerLayout.setDrawerListener(mDrawerToggle);

		} catch (Exception e) {

		}
	}

	private void logout() {
		try {
			Global.clear_shared_preferences(curr_activity, curr_context);

			Intent intent = new Intent(curr_context, Activity_Login.class);
			startActivity(intent);
			finish();
		} catch (Exception e) {

		}
	}

	private void syncData() {
		Async_doSync async_doSync = new Async_doSync(curr_activity, curr_context);
		async_doSync.setOnResultsListener(new AsyncReusable() {

			@Override
			public void onResultsSucceeded(Object result) {
				ui_ToastMessage.getInstance().makeToast(curr_activity, result.toString());

			}
		});
		async_doSync.execute();

	}

	@Override
	public void onBackPressed() {
		showAppKillDialog();
	}

	private void showAppKillDialog() {
		// showing the dialog when user want to exit the application
		Builder builder = new Builder(curr_context);
		builder.setTitle(getString(R.string.str_dlg_ttl_warning));
		builder.setMessage(getString(R.string.str_dlg_msg_app_exit));
		builder.setPositiveButton(getString(R.string.str_dlg_yes), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				curr_activity.finish();
				dialog.dismiss();
			}
		});
		builder.setNegativeButton(getString(R.string.str_dlg_no), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.show();
	}
}
