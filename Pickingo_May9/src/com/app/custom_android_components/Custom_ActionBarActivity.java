package com.app.custom_android_components;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

public class Custom_ActionBarActivity extends ActionBarActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Setting Fragment background
		{/*
			Drawable appThemeBackground = getResources().getDrawable( R.drawable.app_drawer_background );
			getActionBar().setBackgroundDrawable(appThemeBackground);
		*/}

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		switch (item.getItemId()) 
		{
		case android.R.id.home:
			finish();
			return true;    
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
