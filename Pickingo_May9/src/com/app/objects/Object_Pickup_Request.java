package com.app.objects;

import java.io.File;
import java.io.Serializable;
import java.util.List;

public class Object_Pickup_Request implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 0L;
	public int id;
	public String client_order_number;
	public String client_request_id;
	public String aasm_state;
	public String date_created;
	public String weight;
	public String length;
	public String breadth;
	public String height;
	public String scheduled_date;
	public String remarks;
	public String barcode;
	public String hub_name;
	public String client_name;
	public File signature = null;

	public File getSignature() {
		return signature;
	}

	public void setSignature(File signature) {
		this.signature = signature;
	}

	public String getClient_name() {
		return client_name;
	}

	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}

	public List<Skus> skus;
	public Address address;

	public class Skus implements Serializable {
		public int id;
		public String name;
		public boolean picked;
		public boolean selected=false;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public boolean getPicked() {
			return picked;
		}

		public void setPicked(boolean picked) {
			this.picked = picked;
		}

	}

	public class Address implements Serializable {
		public String address_line;
		public String city;
		public String country;
		public String pincode;
		public String name;
		public String phone_number;
		public String locality;
		public String sub_locality;
		public int id;

		public String getAddress_line() {
			return address_line;
		}

		public void setAddress_line(String address_line) {
			this.address_line = address_line;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getPincode() {
			return pincode;
		}

		public void setPincode(String pincode) {
			this.pincode = pincode;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getPhone_number() {
			return phone_number;
		}

		public void setPhone_number(String phone_number) {
			this.phone_number = phone_number;
		}

		public String getLocality() {
			return locality;
		}

		public void setLocality(String locality) {
			this.locality = locality;
		}

		public String getSub_locality() {
			return sub_locality;
		}

		public void setSub_locality(String sub_locality) {
			this.sub_locality = sub_locality;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

	}

	// public String preferred_date;
	// public String clientid;
	// public String clientname;
	// public String client_created_at;
	// public String client_updated_at;
	// public String picked_date;
	// public String volumetric_weight;
	// public String request_type;
	// public String status;
	// public String pickup_boy_id;
	// public String pickup_boy;
	// public String address_line;
	// public String city;
	// public String country;
	// public String pincode;
	// public String name;
	// public String phone_number;
	// // public String skus;
	// public String ispicked;
	// public String skuid;
	// public String userid;
	// public String client;
	// public String accesstoken;
	// public String tokentype;
	// public String uid;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClient_order_number() {
		return client_order_number;
	}

	public void setClient_order_number(String client_order_number) {
		this.client_order_number = client_order_number;
	}

	public String getClient_request_id() {
		return client_request_id;
	}

	public void setClient_request_id(String client_request_id) {
		this.client_request_id = client_request_id;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getDate_created() {
		return date_created;
	}

	public void setDate_created(String date_created) {
		this.date_created = date_created;
	}

	public String getHub() {
		return hub_name;
	}

	public void setHub(String hub) {
		this.hub_name = hub;
	}

	public String getScheduled_date() {
		return scheduled_date;
	}

	public void setScheduled_date(String scheduled_date) {
		this.scheduled_date = scheduled_date;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getBreadth() {
		return breadth;
	}

	public void setBreadth(String breadth) {
		this.breadth = breadth;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getAasm_state() {
		return aasm_state;
	}

	public void setAasm_state(String aasm_state) {
		this.aasm_state = aasm_state;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getHub_name() {
		return hub_name;
	}

	public void setHub_name(String hub_name) {
		this.hub_name = hub_name;
	}

	public List<Skus> getSkus() {
		return skus;
	}

	public void setSkus(List<Skus> skus) {
		this.skus = skus;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
