	package com.app.objects;

import java.util.ArrayList;


public class Object_Login {

	private Object_Header obj_header = new Object_Header();
	private data data = new data();
	private ArrayList<String> al_errors = new ArrayList<String>();
	
	public class data{
		private String uid;
		private String email;
		private String id;
		private String name;
		private String phone;
		private String hub_id;
		private String provider;
		private String nickname;
		private String image;
		private String active;
		public String getUid() {
			return uid;
		}
		public void setUid(String uid) {
			this.uid = uid;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		public String getHub_id() {
			return hub_id;
		}
		public void setHub_id(String hub_id) {
			this.hub_id = hub_id;
		}
		public String getProvider() {
			return provider;
		}
		public void setProvider(String provider) {
			this.provider = provider;
		}
		public String getNickname() {
			return nickname;
		}
		public void setNickname(String nickname) {
			this.nickname = nickname;
		}
		public String getImage() {
			return image;
		}
		public void setImage(String image) {
			this.image = image;
		}
		public String getActive() {
			return active;
		}
		public void setActive(String active) {
			this.active = active;
		}
	}

	public data getData() {
		return data;
	}

	public void setData(data data) {
		this.data = data;
	}

	public ArrayList<String> getAl_errors() {
		return al_errors;
	}

	public void setAl_errors(ArrayList<String> al_errors) {
		this.al_errors = al_errors;
	}

	public Object_Header getObj_header() {
		return obj_header;
	}

	public void setObj_header(Object_Header obj_header) {
		this.obj_header = obj_header;
	}
	
	
}
