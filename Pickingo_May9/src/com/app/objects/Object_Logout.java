package com.app.objects;


public class Object_Logout {

	private ok ok = new ok();
	
	public class ok{
		private String code;
		private String http_code;
		private String message;
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getHttp_code() {
			return http_code;
		}
		public void setHttp_code(String http_code) {
			this.http_code = http_code;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
	}

	public ok getOk() {
		return ok;
	}

	public void setOk(ok ok) {
		this.ok = ok;
	}
}
