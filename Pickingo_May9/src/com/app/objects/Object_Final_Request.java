package com.app.objects;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.app.objects.Object_Pickup_Request.Skus;
import com.global.GlobalAasmStatus;

public class Object_Final_Request {
	public int id;
	public String aasm_state;
	public String scheduled_date;
	public String remarks;
	public String barcode;
	// public File signature = null;
	public List<Skus> skus_attributes;

	public Object_Final_Request(Object_Pickup_Request object_Pickup_Request) {
		this.id = object_Pickup_Request.id;
		this.aasm_state = object_Pickup_Request.aasm_state;
		this.scheduled_date = object_Pickup_Request.scheduled_date;
		this.remarks = object_Pickup_Request.remarks;
		this.barcode = object_Pickup_Request.barcode;
		// this.signature = object_Pickup_Request.signature;
		List<com.app.objects.Object_Pickup_Request.Skus> skusList = object_Pickup_Request.getSkus();
		skus_attributes = new ArrayList<Object_Final_Request.Skus>();
		if (aasm_state.contentEquals(GlobalAasmStatus.KEY_STATUS_PICKED)) {
			int size = skusList.size();
			for (int i = 0; i < size; i++) {
				Skus object = new Skus();
				object.id = skusList.get(i).getId();
				object.picked = skusList.get(i).selected;
				skus_attributes.add(i, object);
			}
		}
	}

	public class Skus implements Serializable {
		public int id;
		public boolean picked;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public boolean getPicked() {
			return picked;
		}

		public void setPicked(boolean picked) {
			this.picked = picked;
		}

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAasm_state() {
		return aasm_state;
	}

	public void setAasm_state(String aasm_state) {
		this.aasm_state = aasm_state;
	}

	public String getScheduled_date() {
		return scheduled_date;
	}

	public void setScheduled_date(String scheduled_date) {
		this.scheduled_date = scheduled_date;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	// public File getSignature() {
	// return signature;
	// }
	//
	// public void setSignature(File signature) {
	// this.signature = signature;
	// }

	public List<Skus> getSkus() {
		return skus_attributes;
	}

	public void setSkus(List<Skus> skus) {
		this.skus_attributes = skus;
	}

}
