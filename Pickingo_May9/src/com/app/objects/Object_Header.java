package com.app.objects;

public class Object_Header {
	
	public static final String KEY_client = "client";
	public static final String KEY_access_token = "access-token";
	public static final String KEY_token_type = "token-type";
	public static final String KEY_uid = "uid";
	
	private String client;
	private String access_token;
	private String token_type;
	private String uid;
	
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	public String getToken_type() {
		return token_type;
	}
	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}



}
