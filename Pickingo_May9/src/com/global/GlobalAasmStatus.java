package com.global;

public interface GlobalAasmStatus {
	String KEY_STATUS_NEW = "new";
	String KEY_STATUS_ASSIGNED = "assigned";
	String KEY_STATUS_OUT_FOR_PICKUP = "out_for_pickup";
	String KEY_STATUS_NOT_CONTACTABLE = "not_contactable";
	String KEY_STATUS_NOT_ATTEMPTED = "not_attempted";
	String KEY_STATUS_CANCELLED = "cancelled";
	String KEY_STATUS_ON_HOLD = "on_hold";
	String KEY_STATUS_CID = "cid";
	String KEY_STATUS_PICKED = "picked";
	String KEY_STATUS_RECIEVED = "received";
	String KEY_STATUS_IN_TRANSIT = "in_transit";
	String KEY_STATUS_CLOSED = "closed";

	
	int RESULT_CODE_DONE=-1111;
}
