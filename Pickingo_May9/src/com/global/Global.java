package com.global;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import me.drakeet.materialdialog.MaterialDialog;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActionBar.LayoutParams;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.app.objects.Object_Login;
import com.app.objects.Object_Logout;
import com.app.objects.Object_Pickup_Request;
import com.app.objects.Object_Summary;
import com.app.pickingo.pickboy.Activity_Login;
import com.app.pickingo.pickboy.R;
import com.global.sharedPreferences.SharedPreferences_Bean;
import com.global.sharedPreferences.SharedPreferences_custom;
import com.global.ui.ToastDialog;
import com.google.gson.Gson;

public class Global implements GlobalAasmStatus {

	public static final String KEY_ERROR_MESSAGE = "Oopss! \nWe are facing some error at this moment. Please try again later";

	public static final String KEY_APP_VERSION_CODE = "versionCode";
	public static final String KEY_APP_VERSION_NAME = "versionName";

	public static final String MALE = "male";
	public static final String FEMALE = "female";

	public static final String TRUE = "true";
	public static final String FALSE = "false";

	public static final String KEY_NONE = " == None == ";

	public static boolean isSdCardPresent() {
		return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);

	}

	public static String getExternalDirectoryFolder() {
		return Environment.getExternalStorageDirectory() + "/Hello1/";
	}

	public static String getExternalSignatureDirectoryPath() {
		return Environment.getExternalStorageDirectory().getPath() + "/Pickingo/Signatures";
	}

	DialogInterface.OnClickListener dialogListener_error;

	public void AlertInternetNotConnected(final Activity activity, final boolean isToFinish) {
		dialogeListener(activity);

		ToastDialog dialog = new ToastDialog(activity, "Internet !!", "Internet Not Available.", R.drawable.ic_icon_alert_exclamation, true, "OK", null,
				isToFinish ? dialogListener_error : null);
		dialog.showDialog();
	}

	public void AlertMaterial_InternetNotConnected(final Activity activity) {
		try {

			final MaterialDialog mMaterialDialog = new MaterialDialog(activity);

			if (mMaterialDialog != null) {
				mMaterialDialog.setTitle("Internet Problem !!").setMessage("Internet Not Available. Please try to connect your phone to Mobile data or Wi-Fi.")
				// mMaterialDialog.setBackgroundResource(R.drawable.background);
						.setPositiveButton("OK", new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								mMaterialDialog.dismiss();
							}
						})

						.setCanceledOnTouchOutside(false).setOnDismissListener(new DialogInterface.OnDismissListener() {
							@Override
							public void onDismiss(DialogInterface dialog) {
							}
						}).show();
			}
		} catch (Exception e) {
			System.out.println();
		}
	}

	private void dialogeListener(final Activity activity) {
		dialogListener_error = new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// Write your code here to execute after dialog

				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					activity.finish();
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					break;

				}
				dialog.cancel();
			}
		};
	}

	public static boolean isServiceRunning(final Context context, Class<?> serviceClass) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceClass.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	public static void clear_shared_preferences(final Activity curr_activity, final Context curr_context) {
		try {
			for (int index = 0; index < SharedPreferences_Bean.Array_KEY_SHARED_PREFERENCES.length; index++) {
				/*
				 * Shared Preferences
				 */
				SharedPreferences_custom obj_sp = new SharedPreferences_custom(curr_activity, SharedPreferences_Bean.Array_KEY_SHARED_PREFERENCES[index]);
				obj_sp.clearSharedPreferences();
			}

			Intent intent = new Intent(curr_context, Activity_Login.class);
			curr_context.startActivity(intent);

			curr_activity.finish();
		} catch (Exception e) {

		}
	}

	/*
	 * Capturing Camera Image will lauch camera app requrest image capture
	 */
	// Activity request codes
	public static final int CAMERA_CAPTURE_CAMERA_IMAGE_REQUEST_CODE = 100;
	public static final int CAMERA_CAPTURE_GALLERY_IMAGE_REQUEST_CODE = 101;
	public static final int GALLARY_GET_MULTIPLE_IMAGES_REQUEST_CODE = 103;
	public static final int CROP_PIC_REQUEST_CODE = 104;
	public static final int CODE_PICK_CHAIN_OF = 105;

	public static final int MEDIA_TYPE_IMAGE = 1;

	// directory name to store captured images and videos
	private static final String IMAGE_DIRECTORY_NAME = "Hello Camera";

	private Uri fileUri; // file url to store image/video

	/*
	 * Creating file uri to store image/video
	 */
	public static Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/*
	 * returning image / video
	 */
	private static File getOutputMediaFile(int type) {

		// External sdcard location
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create " + IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
		} else {
			return null;
		}

		return mediaFile;
	}

	public static String getImagePath(Context inContext, Bitmap inImage, String title) {
		try {
			/*
			 * ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			 * inImage.compress(Bitmap.CompressFormat.JPEG, 0, bytes);
			 */
			String path = Images.Media.insertImage(inContext.getContentResolver(), inImage, title, null);
			return path;
		} catch (Exception e) {
			return "";
		}
	}

	public static boolean isAutologin(final Activity curr_activity) {

		boolean isLoggedIn = false;
		try {
			/*
			 * Shared Preferences
			 */
			SharedPreferences_custom obj_sp_login = new SharedPreferences_custom(curr_activity, SharedPreferences_Bean.KEY_LOGIN_DETAILS);

			String loginJson = obj_sp_login.getString(SharedPreferences_Bean.KEY_LOGIN_DETAILS);

			final Object_Login obj_login = new Gson().fromJson(loginJson, Object_Login.class);

			if ((obj_login != null) && (obj_login.getData() != null) && (obj_login.getData().getUid().length() > 0)) {
				isLoggedIn = true;
			}
		} catch (Exception e) {

		}

		// return false;
		return isLoggedIn;
	}

	public static Object_Login getLoginInfo(final Activity curr_activity) {

		Object_Login obj_login = null;
		try {
			/*
			 * Shared Preferences
			 */
			SharedPreferences_custom obj_sp_login = new SharedPreferences_custom(curr_activity, SharedPreferences_Bean.KEY_LOGIN_DETAILS);

			String loginJson = obj_sp_login.getString(SharedPreferences_Bean.KEY_LOGIN_DETAILS);

			obj_login = new Gson().fromJson(loginJson, Object_Login.class);
		} catch (Exception e) {

		}

		// return false;
		return obj_login == null ? new Object_Login() : obj_login;
	}

	public static String getLoggedIn_UserId(final Activity curr_activity) {

		Object_Login obj_login = null;
		try {
			/*
			 * Shared Preferences
			 */
			SharedPreferences_custom obj_sp_login = new SharedPreferences_custom(curr_activity, SharedPreferences_Bean.KEY_LOGIN_DETAILS);

			String loginJson = obj_sp_login.getString(SharedPreferences_Bean.KEY_LOGIN_DETAILS);

			obj_login = new Gson().fromJson(loginJson, Object_Login.class);
		} catch (Exception e) {

		}

		// return false;
		return obj_login != null ? obj_login.getData().getUid() : null;
	}

	public static String getLoggedIn_UserEmail(final Activity curr_activity) {

		Object_Login obj_login = null;
		try {
			/*
			 * Shared Preferences
			 */
			SharedPreferences_custom obj_sp_login = new SharedPreferences_custom(curr_activity, SharedPreferences_Bean.KEY_LOGIN_DETAILS);

			String loginJson = obj_sp_login.getString(SharedPreferences_Bean.KEY_LOGIN_DETAILS);

			obj_login = new Gson().fromJson(loginJson, Object_Login.class);
		} catch (Exception e) {

		}

		// return false;
		return obj_login != null ? obj_login.getData().getEmail() : null;
	}

	public static void showpopup_Album(final Activity curr_activity, final Context curr_context) {

		LayoutInflater inflater = (LayoutInflater) curr_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = inflater.inflate(R.layout.menu_popup_window_layout, null, false);

		TextView tv_popup_header_text = (TextView) popupView.findViewById(R.id.tv_popup_header_text);

		ListView lv_popup_menu = (ListView) popupView.findViewById(R.id.lv_popup_menu);

		String[] items = curr_activity.getResources().getStringArray(R.array.array_image_pic_from);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(curr_context, android.R.layout.simple_list_item_1, items);

		lv_popup_menu.setAdapter(adapter);

		final PopupWindow pw = new PopupWindow(popupView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
		pw.setOutsideTouchable(true);
		pw.setFocusable(true);
		pw.setBackgroundDrawable(new BitmapDrawable());
		pw.setTouchInterceptor(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
					pw.dismiss();

					return true;
				}

				return false;
			}
		});

		lv_popup_menu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (position == 0) {
					pw.dismiss();
				} else if (position == 1) {
					pw.dismiss();
				} else if (position == 2) {
					pw.dismiss();
				}
			}
		});
		pw.showAtLocation(popupView, Gravity.CENTER, 0, 0);
	}

	public static String Object_GsonString(Object object) {
		return new Gson().toJson(object);
	}

	public static Object String_GsonObject(String jsonString, Class<?> object_class) {
		return new Gson().fromJson(jsonString, object_class);
	}

	public static String getTextCorrespondToAssm_State(String assm_state) {
		String state = null;
		if (assm_state.contentEquals(KEY_STATUS_CANCELLED)) {
			state = "CANCELLED";
		} else if (assm_state.contentEquals(KEY_STATUS_OUT_FOR_PICKUP)) {
			state = "PENDING";
		} else if (assm_state.contentEquals(KEY_STATUS_PICKED)) {
			state = "PICKED UP";
		} else if (assm_state.contentEquals(KEY_STATUS_NOT_CONTACTABLE)) {
			state = "NOT CONTACTABLE";
		} else if (assm_state.contentEquals(KEY_STATUS_CID)) {
			state = "POSTPONED";
		}
		return state;
	}

}
