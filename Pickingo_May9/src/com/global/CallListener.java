package com.global;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;

public class CallListener implements OnClickListener {
	String mPhoneNumberString=null;
	Context mCurrentContext;

	public CallListener(Context context,String phoneNumber) {
		mCurrentContext=context;
		mPhoneNumberString = phoneNumber;
	}

	@Override
	public void onClick(View v) {
		//Starting the call Intent to the given number
		Intent intent = new Intent(Intent.ACTION_CALL);
		intent.setData(Uri.parse("tel:"+mPhoneNumberString));
		mCurrentContext.startActivity(intent);
	}

}
