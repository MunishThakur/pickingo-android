package com.global.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class ShadowDrawable extends Drawable {

	private float x;
	private float y;
	private float radius;
	private int color = Color.BLACK;
	private Rect rect;
	private Paint mShadow;
	private int oneDp;
	
	public ShadowDrawable(Context context) {
		x = 0.0F;
		y = 0.0F;
		radius = ScreenHelper.getDPToPixel(context, 5);
		oneDp = ScreenHelper.getDPToPixel(context, 1);
		mShadow = new Paint();
		rect = new Rect();
	}
	
	@Override
	public void draw(Canvas canvas) {
		
		getPadding(rect);
		
		Rect bounds = getBounds();
		
		mShadow.setShadowLayer(radius, x, y, color);
		mShadow.setStrokeWidth(oneDp);
		mShadow.setStyle(Style.FILL);
		canvas.translate(rect.left, rect.top);
		mShadow.setColor(color);
		
		rect.left += bounds.left - oneDp;
		rect.top += bounds.top - oneDp;
		rect.right = bounds.right - rect.right + oneDp;
		rect.bottom = bounds.bottom - rect.bottom + oneDp;
		
		canvas.drawRect(rect, mShadow);
		
		mShadow.setShadowLayer(0, 0, 0, 0);
		mShadow.setStyle(Style.FILL);
		
		int color = this.color;
		int red = Color.red(color);
		int green = Color.green(color);
		int blue = Color.blue(color);
		
		double lightness = 0.5;
		color = Color.argb(255, 255 - (int) ((255 - red) * lightness), 255 - (int) ((255 - green) * lightness), 255 - (int) ((255 - blue) * lightness));
		
		mShadow.setColor(Color.WHITE);
		
		canvas.drawRect(rect, mShadow);
	}

	@Override
	public void setAlpha(int alpha) {

	}
	
	public Paint getPaint() {
		return mShadow;
	}

	@Override
	public void setColorFilter(ColorFilter cf) {

	}

	@Override
	public int getOpacity() {
		return PixelFormat.TRANSLUCENT;
	}

	@Override
	public boolean getPadding(Rect padding) {
		int left = (int) ((radius / 2) - x) + oneDp;
		int top = (int) ((radius / 2) - y) + oneDp;
		if(left < 0)
			left = 0;
		if(top < 0)
			top = 0;
		int right = left + (int) (radius + x) + oneDp;
		int bottom = top + (int) (radius + y) + oneDp;
		
		padding.set(left, top, right, bottom);
		return true;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

}
