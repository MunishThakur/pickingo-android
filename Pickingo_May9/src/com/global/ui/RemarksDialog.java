package com.global.ui;


import android.content.Context;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;

import com.gc.materialdesign.views.ButtonFlat;
import com.gc.materialdesign.widgets.Dialog;

public class RemarksDialog {

	Context curr_Context;
	String title;
	String message;
	static RemarksDialog mRemarksDialog;
	static Dialog mDialog;

	private RemarksDialog(Context context, String title, String message) {
		curr_Context=context;
		this.title=title;
		this.message=message;
	}

	public static RemarksDialog getInstance(Context context, String title, String message) {
		if (mRemarksDialog == null)
			mRemarksDialog = new RemarksDialog(context, title, message);
		return mRemarksDialog;
	}
	public void showDialog(){
		if(mDialog==null){
			mDialog=new Dialog(curr_Context, title, message);
		}
		EditText editText=new EditText(curr_Context);
		LayoutParams params=new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
		editText.setMinLines(7);
		mDialog.addContentView(editText, params);
		ButtonFlat buttonFlat=new ButtonFlat(curr_Context, null);
		mDialog.setButtonAccept(buttonFlat);
		mDialog.show();
		
	}

}
