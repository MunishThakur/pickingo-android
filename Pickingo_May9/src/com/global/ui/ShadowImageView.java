package com.global.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ShadowImageView extends ImageView {

	private ShadowDrawable shadow;
	
	public ShadowImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public ShadowImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public ShadowImageView(Context context) {
		super(context);
		init(context);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void init(Context context) {
		shadow = new ShadowDrawable(context);
		setBackgroundDrawable(shadow);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
			setLayerType(LAYER_TYPE_SOFTWARE, null);
	}


	public void setShadowX(float x) {
		shadow.setX(x);
	}

	public void setShadowY(float y) {
		shadow.setY(y);
	}

	public void setShadowRadius(float radius) {
		shadow.setRadius(radius);
	}

	public void setShadowColor(int color) {
		shadow.setColor(color);
	}
	
}
