package com.global.ui;

import java.util.HashMap;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

/**
 * 
 * @author Munish Thakur
 *
 */

/** Customizing AutoCompletecom.app.views.UI_Custom_TextView_OpenSens to return Place Description   
 *  corresponding to the selected item
 */
public class ui_CustomAutoCompleteTextView extends AutoCompleteTextView {
	
	public ui_CustomAutoCompleteTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/** Returns the place description corresponding to the selected item */
	@Override
	protected CharSequence convertSelectionToString(Object selectedItem) {
		/** Each item in the autocompetecom.app.views.UI_Custom_TextView_OpenSens suggestion list is a hashmap object */
		HashMap<String, String> hm = (HashMap<String, String>) selectedItem;
		return hm.get("description");
	}
}
