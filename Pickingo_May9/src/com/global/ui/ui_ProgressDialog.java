package com.global.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

public class ui_ProgressDialog{

	private Activity activity;
	private Context context;
	
	public ProgressDialog prog;
	
	public ui_ProgressDialog() {

	}

	public void makeProgressDialog(final Activity activity, final Context context, boolean isCancelable){
		
		this.activity = activity;
		this.context = context;
		
		prog = new ProgressDialog(activity);
		prog.setMessage("Please Wait...");
		prog.setCancelable(isCancelable);
	}
	
	public void show(){
		
		if((prog != null) && !(isShowing())){
			prog.show();
		}
	}
	
	public void dismiss(){
		if((prog != null) && (isShowing())){
			prog.dismiss();
		}
	}
	
	public boolean isShowing(){
		if((prog.isShowing())){
			return true;
		} else{
			return false;
		}
	}
	
	public void setMessage(final String message){
		prog.setMessage(message);
	}
	
	private static ui_ProgressDialog uniqInstance;
	
	public static synchronized ui_ProgressDialog getInstance() {
		if (uniqInstance == null) {
			uniqInstance = new ui_ProgressDialog();
		}
		return uniqInstance;
	}
}
