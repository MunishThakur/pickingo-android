package com.global.ui;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;

public class ScreenHelper {

	public static final int MIN_WIDTH_FOR_LARGE_SCREEN = 400;
	
	public static int getPixelToDP(DisplayMetrics metrics, int pixel) {
		return (pixel * 160) / metrics.densityDpi;
	}
	
	// Old Source
	// public static int getDPToPixel(DisplayMetrics metrics, int dp) {
	// return (int) (dp * metrics.densityDpi) / 160;
	// }
	
	public static int getDPToPixel(DisplayMetrics metrics, int dp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
	}
	
	public static int getDPToPixel(Context context, int dp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
	}
	
	public static int getSPToPixel(DisplayMetrics metrics, int sp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, metrics);
	}

	public static int getWidthInDp(DisplayMetrics metrics) {
		return getPixelToDP(metrics, metrics.widthPixels);
	}

	public static int getHeightInDp(DisplayMetrics metrics) {
		return getPixelToDP(metrics, metrics.heightPixels);
	}

	public static int getPixelToDP(Context context, int pixel) {
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager(context).getDefaultDisplay().getMetrics(metrics);
		return getPixelToDP(metrics, pixel);
	}

	public static int getWidthInDp(Context context) {
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager(context).getDefaultDisplay().getMetrics(metrics);
		return getPixelToDP(metrics, metrics.widthPixels);
	}

	public static int getHeightInDp(Context context) {
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager(context).getDefaultDisplay().getMetrics(metrics);
		return getPixelToDP(metrics, metrics.heightPixels);
	}
	
	public static WindowManager getWindowManager(Context context)
	{
		return (android.view.WindowManager) context.getSystemService (android.content.Context.WINDOW_SERVICE);
	}
	
	public static boolean isLargeScreen(Context context)
	{
		DisplayMetrics metrics = getDisplayMetrics(context);
		return getWidthInDp(metrics) >= MIN_WIDTH_FOR_LARGE_SCREEN && getHeightInDp(metrics) >= MIN_WIDTH_FOR_LARGE_SCREEN;
	}
	
	public static Display getDefaultDisplay(Context context)
	{
		return getWindowManager(context).getDefaultDisplay();
	}
	
	public static DisplayMetrics getDisplayMetrics(Context context)
	{
		DisplayMetrics metrics = new DisplayMetrics();
		getDefaultDisplay(context).getMetrics(metrics);
		return metrics;
	}
	
	public static boolean isLandscapeOrientation(Context context)
	{
		return getOrientation(context) == Configuration.ORIENTATION_LANDSCAPE;
	}
	
	public static int getOrientation(Context context)
	{
		Point size = new Point();
		
		DisplayMetrics metrics = getDisplayMetrics(context);
		
		size = new Point(getWidthInDp(metrics), getHeightInDp(metrics));
		
		int orientation = Configuration.ORIENTATION_UNDEFINED;
		
	    if(size.x == size.y){
	        orientation = Configuration.ORIENTATION_SQUARE;
	    } else{ 
	        if(size.x < size.y){
	            orientation = Configuration.ORIENTATION_PORTRAIT;
	        }else { 
	             orientation = Configuration.ORIENTATION_LANDSCAPE;
	        }
	    }
	    
	    return orientation;
	}
	
	
}