package com.global.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.Toast;

public class ToastDialog {
	/** Called when the activity is first created. */

	private Context context;
	private AlertDialog.Builder alertDialog = null;
	private OnClickListener dialogListener;

	private String title, message, positiveText, negativeText;
	private int icon;
	private boolean isCancelable;

	public ToastDialog(Context context, String title, String message,
			int icon, boolean isCancelable, String positiveText,
			String negativeText, OnClickListener dialogListener) {
		this.context = context;
		this.title = title;
		this.message = message;
		this.icon = icon;
		this.isCancelable = isCancelable;
		this.positiveText = positiveText;
		this.negativeText = negativeText;
		this.dialogListener = dialogListener;
	}

	public void showDialog() {

		alertDialog = new AlertDialog.Builder(context);
		alertDialog.setCancelable(isCancelable);

		if (title != null && title.length() > 0) {
			alertDialog.setTitle(title);
		}

		if (message != null && message.length() > 0) {
			alertDialog.setMessage(message);
		}

		if (icon != 0) {
			alertDialog.setIcon(icon);
		}

		if (positiveText != null && positiveText.length() > 0) {
			// Setting Positive "Yes" Button
			alertDialog.setPositiveButton(positiveText, dialogListener);
		}
		// Setting Negative "NO" Button
		if (negativeText != null && negativeText.length() > 0) {
			alertDialog.setNegativeButton(negativeText, dialogListener);
		}

		alertDialog.show();
		// Showing Alert Message
	}

}