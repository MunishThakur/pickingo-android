package com.global.ui;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.widget.TextView;
import android.widget.TextView.BufferType;

public class RequiredTextView extends TextView {

	public RequiredTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
	}

	public RequiredTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
	}

	public RequiredTextView(Context context) {
		super(context);
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
		SpannableStringBuilder builder = new SpannableStringBuilder(text);
		builder.append(" *");
		builder.setSpan(new ForegroundColorSpan(Color.RED), builder.length() - 1, builder.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
		super.setText(builder, type);
	}
	
}
