package com.global.ui;

import android.app.Activity;
import android.widget.Toast;

public class ui_ToastMessage{

	public ui_ToastMessage() {

	}

	public void makeToast(final Activity activity, String Message){
		Toast.makeText(activity, Message, Toast.LENGTH_LONG).show();
	}
	
	private static ui_ToastMessage uniqInstance;
	
	public static synchronized ui_ToastMessage getInstance() {
		if (uniqInstance == null) {
			uniqInstance = new ui_ToastMessage();
		}
		return uniqInstance;
	}
}
