package com.global.ui;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.widget.TextView;
import android.widget.TextView.BufferType;

public class UnderLineTextView extends TextView {

	public UnderLineTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
	}

	public UnderLineTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
	}

	public UnderLineTextView(Context context) {
		super(context);
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
		SpannableStringBuilder builder = new SpannableStringBuilder(text);
		builder.setSpan(new ForegroundColorSpan(Color.RED), 0, builder.length(), 0);
		builder.setSpan(new UnderlineSpan(), 0, builder.length(), 0);
		
		
		builder.setSpan(new StyleSpan(Typeface.BOLD), 0, builder.length(), 0);
		//builder.setSpan(new StyleSpan(Typeface.ITALIC), 0, builder.length(), 0);
		
		//this.setTypeface(Typeface.createFromAsset(getContext().getResources().getAssets(), "Ubuntu-R.ttf"));
		super.setText(builder, type);
	}
	
}
