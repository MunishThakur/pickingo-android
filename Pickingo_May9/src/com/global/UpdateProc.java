package com.global;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.app.connection.WS_Calls;
import com.app.database.DatabaseManager;
import com.app.objects.Object_Final_Request;
import com.app.objects.Object_Login;
import com.app.objects.Object_Pickup_Request;
import com.app.objects.Object_Pickup_Request.Skus;
import com.app.pickingo.pickboy.R;
import com.asyncTasks.AsyncReusable;
import com.asyncTasks.Async_doUpdateRequest;
import com.global.network.NetworkUtil;
import com.global.ui.ui_ToastMessage;

public class UpdateProc implements GlobalAasmStatus {
	private static final String TAG = "Update_proc";
	static DatabaseManager databaseManager;
	static String userId;
	private static final NetworkUtil obj_netwok_util = new NetworkUtil();

	public static void initUpdateProcess(final Activity curr_activity, Context curr_context, Object_Pickup_Request pickup_Request) {

		try {
			databaseManager = DatabaseManager.getInstance(curr_context);
			userId = Global.getLoginInfo(curr_activity).getObj_header().getUid();
			// initiating database query
			int resultValue = databaseManager.updatepickupRequest(userId, pickup_Request);
			final String aasm_state=pickup_Request.getAasm_state();

			if (resultValue == 1) {
				// query successful
				// Initiating update request for server
				Async_doUpdateRequest async_doUpdateRequest = new Async_doUpdateRequest(curr_activity, curr_context, pickup_Request);
				async_doUpdateRequest.setOnResultsListener(new AsyncReusable() {

					@Override
					public void onResultsSucceeded(Object result) {
						Object_Pickup_Request pickup_Request = (Object_Pickup_Request) result;

						if (pickup_Request != null && pickup_Request.getId() > 0) {
							ui_ToastMessage.getInstance().makeToast(curr_activity, getRequestSuccessMessage(curr_activity,aasm_state));
							curr_activity.setResult(RESULT_CODE_DONE);
							curr_activity.finish();
						}
					

					}
				});
				async_doUpdateRequest.execute();
			} else {
				Log.e(TAG, "database entry failed");
				new Global().AlertMaterial_InternetNotConnected(curr_activity);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public static Object_Pickup_Request main_Update_Proc(Context curr_context, Activity curr_activity, Object_Pickup_Request object_Pickup_Request) {

		try {

			Object_Pickup_Request responseData = upload_Raw_Data(curr_context, curr_activity, object_Pickup_Request);
			if (responseData != null && responseData.aasm_state.contentEquals(object_Pickup_Request.aasm_state)) {
				String userId = Global.getLoginInfo(curr_activity).getObj_header().getUid();
				// sync the database
				if(databaseManager==null)
					databaseManager = DatabaseManager.getInstance(curr_context);
				if (databaseManager.updateSyncStatus(true, userId, object_Pickup_Request) == 1) {
					// uploading the image.
					if (object_Pickup_Request.getAasm_state() == KEY_STATUS_PICKED) {
						Object_Pickup_Request fileResponseData = upload_File_Proc(curr_context, curr_activity, object_Pickup_Request);
						if (fileResponseData != null && fileResponseData.aasm_state.contentEquals(object_Pickup_Request.aasm_state)) {
							Log.e("sign", "sign uploaded succesfully");
						}

						return responseData;
					}
				} else {
					Log.e("TAG", "sync status update failed");
					return new Object_Pickup_Request();
				}

			}
		} catch (Exception e) {

		}
		return null;
	}

	private static Object_Pickup_Request upload_Raw_Data(Context curr_context, Activity curr_activity, Object_Pickup_Request object_Pickup_Request) {
		try {
			WS_Calls obj_call_soap = new WS_Calls();
			if (obj_netwok_util.isInternetConnected(curr_context)) {
				// File file_sign = object_Pickup_Request.getSignature();
				Object_Final_Request final_Request = new Object_Final_Request(object_Pickup_Request);
				Object_Login object_Login = Global.getLoginInfo(curr_activity);
				Object_Pickup_Request pickup_Request = obj_call_soap.ws_PutUpdateRequests(object_Login, final_Request);
				return pickup_Request;
			}
		} catch (Exception e) {
			Log.e("error", e.toString());
		}
		return null;
	}

	private static Object_Pickup_Request upload_File_Proc(Context curr_context, Activity curr_activity, Object_Pickup_Request object_Pickup_Request) {
		try {
			WS_Calls obj_call_soap = new WS_Calls();
			if (obj_netwok_util.isInternetConnected(curr_context)) {
				File file_sign = object_Pickup_Request.getSignature();
				Object_Final_Request final_Request = new Object_Final_Request(object_Pickup_Request);
				Object_Login object_Login = Global.getLoginInfo(curr_activity);
				Object_Pickup_Request pickup_Request = obj_call_soap.ws_PutUpdateSignRequests(object_Login, final_Request, file_sign);
				return pickup_Request;
			}
		} catch (Exception e) {
			Log.e("error", e.toString());
		}
		return null;
	}
	
	private static String getRequestSuccessMessage(Activity curr_activity ,String aasm_state){
		String result=curr_activity.getString(R.string.str_tst_request_success);
		String state="";
		if( aasm_state!=null &&  aasm_state.contentEquals(KEY_STATUS_PICKED)){
			state="picked";
		}else if(aasm_state!=null &&  aasm_state.contentEquals(KEY_STATUS_CANCELLED)){
			state="cancelled";
		}else if(aasm_state!=null &&  aasm_state.contentEquals(KEY_STATUS_CID)){
			state="postponed";
		}else if(aasm_state!=null &&  aasm_state.contentEquals(KEY_STATUS_NOT_CONTACTABLE)){
			state="is not contactable";
		}
		
		result=String.format(result, state);
		return result;
	}
}
