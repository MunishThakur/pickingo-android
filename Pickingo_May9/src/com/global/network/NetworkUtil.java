package com.global.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {
	
	private static int TYPE_WIFI = 1;
	private static int TYPE_MOBILE = 2;
	private static int TYPE_NOT_CONNECTED = 0;
	
	private static boolean isInternetConnected = false;
	
	private int getConnectivityStatus(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (null != activeNetwork) {
			if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
				return TYPE_WIFI;
			
			if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
				return TYPE_MOBILE;
		} 
		return TYPE_NOT_CONNECTED;
	}
	
	public String setConnectivityStatusString(Context context) {
		int conn = getConnectivityStatus(context);
		String status = null;
		if (conn == NetworkUtil.TYPE_WIFI) {
			status = "Wifi enabled";
			isInternetConnected = true;
		} else if (conn == NetworkUtil.TYPE_MOBILE) {
			status = "Mobile data enabled";
			isInternetConnected = true;
		} else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
			status = "Not connected to Internet";
			isInternetConnected = false;
		}
		return status;
	}
	
	public boolean isInternetConnected(Context context){
		try{
			setConnectivityStatusString(context);
		} catch(Exception e){
			
		}
		return isInternetConnected;
	}
}
