package com.global.network;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.widget.Toast;

public class GpsAndNetwork {

	static public boolean IsGpsOpen(Context con) {

		boolean flag=true;

		LocationManager locationManager;

		locationManager = (LocationManager) con
				.getSystemService(Context.LOCATION_SERVICE);
		if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

			/*Intent gpsOptionsIntent = new Intent(
					android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			gpsOptionsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			con.startActivity(gpsOptionsIntent);*/

			flag=false;

		}
		/*
		if (!locationManager
				.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

			Intent gpsOptionsIntent = new Intent(
					android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			gpsOptionsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			con.startActivity(gpsOptionsIntent);
			Toast.makeText(con, " enable network location", 1).show();
			flag=false;
		}
		 */
		return flag;

	}

	static public void redirectToSettingGPS(Context con) {

		Intent gpsOptionsIntent = new Intent(
				android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		gpsOptionsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		con.startActivity(gpsOptionsIntent);

	}

}
