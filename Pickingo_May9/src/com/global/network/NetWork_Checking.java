package com.global.network;

import android.app.Activity;

public class NetWork_Checking {

	public boolean netstatus = false;

	public NetWork_Checking(Activity callingActivity) {
		WaitForInternetCallback callback = new WaitForInternetCallback(
				callingActivity) {
			public void onConnectionSuccess() {
				netstatus = true;
			}

			public void onConnectionFailure() {
				netstatus = false;
			}
		};

		try {
			WaitForInternet.setCallback(callback);
		} catch (SecurityException e) {
			netstatus = false;
			// callback.onConnectionSuccess();
		}

	}
}