package com.global.view;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

public class FixedFocusScrollView extends ScrollView {

	public FixedFocusScrollView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
	}

	public FixedFocusScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FixedFocusScrollView(Context context) {
		super(context);
	}

	@Override
	public void requestChildFocus(View child, View focused) {
		// avoid scrolling to focused view
		// super.requestChildFocus(child, focused);
	}

}
