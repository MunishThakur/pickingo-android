package com.asyncTasks;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import com.app.connection.WS_Calls;
import com.app.database.DatabaseManager;
import com.app.objects.Object_Login;
import com.app.objects.Object_Pickup_Request;
import com.global.Global;
import com.global.GlobalAasmStatus;
import com.global.network.NetworkUtil;
import com.global.network.TestConnection;
import com.global.ui.ui_ProgressDialog;
import com.global.ui.ui_ToastMessage;

public class Async_doPickupRequest extends AsyncTask<String, Integer, Object> implements GlobalAasmStatus {
	// Instances of current class
	Activity curr_activity;
	Context curr_context;
	/*
	 * Singelton Classes
	 */
	ui_ProgressDialog obj_prog = ui_ProgressDialog.getInstance();
	ui_ToastMessage obj_toast = ui_ToastMessage.getInstance();

	// Onject of TestConnection class
	TestConnection obj_connection = new TestConnection();

	WS_Calls obj_call_soap = new WS_Calls();

	AsyncReusable listener;
	private final NetworkUtil obj_netwok_util = new NetworkUtil();
	private final Global obj_global = new Global();

	public Async_doPickupRequest(final Activity curr_activity, final Context curr_context) {
		this.curr_activity = curr_activity;
		this.curr_context = curr_context;
	}

	public void setOnResultsListener(AsyncReusable listener) {
		this.listener = listener;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected Object doInBackground(String... params) {
		// fetching the data from API
		if (obj_netwok_util.isInternetConnected(curr_context)) {
			try {
				Object_Login object_Login = Global.getLoginInfo(curr_activity);
				String userId = object_Login.getObj_header().getUid();
				List<Object_Pickup_Request> pickupRequestList = obj_call_soap.ws_GetRequests(object_Login.getData().getId(), object_Login.getObj_header());
				DatabaseManager databaseManager = DatabaseManager.getInstance(curr_context);
				if (pickupRequestList.size() > 0) {
					for (Object_Pickup_Request object_Pickup_Request : pickupRequestList) {
						// adding to database
						databaseManager.insertPickupRequest(userId.trim(), object_Pickup_Request);
					}
					return AsyncReusable.API_REQUEST_SUCCESS;
				}
			} catch (Exception e) {

			}
		}

		return AsyncReusable.API_REQUEST_FAILED;
	}

	@Override
	protected void onPostExecute(Object result) {
		super.onPostExecute(result);
		try {
			if (obj_netwok_util.isInternetConnected(curr_context)) {
				listener.onResultsSucceeded(result);
			} else {
				obj_global.AlertMaterial_InternetNotConnected(curr_activity);
			}
		} catch (Exception e) {
			System.out.println();
		}
	}

}
