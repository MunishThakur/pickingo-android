package com.asyncTasks;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import com.app.connection.WS_Calls;
import com.app.objects.Object_Pickup_Request;
import com.global.Global;
import com.global.GlobalAasmStatus;
import com.global.UpdateProc;
import com.global.network.NetworkUtil;
import com.global.network.TestConnection;
import com.global.ui.ui_ProgressDialog;
import com.global.ui.ui_ToastMessage;

public class Async_doUpdateRequest extends AsyncTask<String, Integer, Object> implements GlobalAasmStatus {
	// Instances of current class
	Activity curr_activity;
	Context curr_context;
	/*
	 * Singelton Classes
	 */
	ui_ProgressDialog obj_prog = ui_ProgressDialog.getInstance();
	ui_ToastMessage obj_toast = ui_ToastMessage.getInstance();

	// Onject of TestConnection class
	TestConnection obj_connection = new TestConnection();

	WS_Calls obj_call_soap = new WS_Calls();

	AsyncReusable listener;
	private final NetworkUtil obj_netwok_util = new NetworkUtil();
	private final Global obj_global = new Global();
	Object_Pickup_Request object_Pickup_Request;

	public Async_doUpdateRequest(final Activity curr_activity, final Context curr_context, Object_Pickup_Request pickup_Request) {
		this.curr_activity = curr_activity;
		this.curr_context = curr_context;
		object_Pickup_Request = pickup_Request;
	}

	public void setOnResultsListener(AsyncReusable listener) {
		this.listener = listener;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
//		obj_toast.makeToast(curr_activity, "Requesting api");
		obj_prog.makeProgressDialog(curr_activity, curr_context, true);
		obj_prog.prog.setMessage("Please Wait...");
		obj_prog.show();
	}

	@Override
	protected Object doInBackground(String... params) {
		return UpdateProc.main_Update_Proc(curr_context, curr_activity, object_Pickup_Request);
	}

	@Override
	protected void onPostExecute(Object result) {
		super.onPostExecute(result);

		try {
			if (obj_netwok_util.isInternetConnected(curr_context)) {
				listener.onResultsSucceeded(result);
			} else {
				obj_toast.makeToast(curr_activity, "Item added to sync queue");
				obj_global.AlertMaterial_InternetNotConnected(curr_activity);
			}
		} catch (Exception e) {
			System.out.println();
		}
		obj_prog.dismiss();
	}

}
