package com.asyncTasks;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import com.app.connection.WS_Calls;
import com.app.objects.Object_Login;
import com.app.objects.Object_Logout;
import com.global.Global;
import com.global.network.NetworkUtil;
import com.global.network.TestConnection;
import com.global.ui.ui_ProgressDialog;
import com.global.ui.ui_ToastMessage;
import com.google.gson.Gson;

public class Async_doLogout extends AsyncTask<String, Integer, Object> {

	// Instances of current class
	Activity curr_activity;
	Context curr_context;

	/*
	 * Singelton Classes
	 */
	ui_ProgressDialog obj_prog = ui_ProgressDialog.getInstance();
	ui_ToastMessage obj_toast = ui_ToastMessage.getInstance();

	// Onject of TestConnection class
	TestConnection obj_connection = new TestConnection();

	WS_Calls obj_call_soap = new WS_Calls();

	AsyncReusable listener;

	String key;

	private final NetworkUtil obj_netwok_util = new NetworkUtil();
	private final Global obj_global = new Global();

	public Async_doLogout(final Activity curr_activity, final Context curr_context, final String key){

		this.curr_activity = curr_activity;
		this.curr_context = curr_context;
		this.key = key;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		obj_prog.makeProgressDialog(curr_activity, curr_context, true);	
		obj_prog.prog.setMessage("Please Wait...");
		obj_prog.show();
	}

	public void setOnResultsListener(AsyncReusable listener) {
		this.listener = listener;
	}

	@Override
	protected Object doInBackground(String... params) {

		Object_Logout obj_result = null;
		try {
			if(obj_netwok_util.isInternetConnected(curr_context)){
				final String resultSoap = obj_call_soap.ws_GetLogout(key);
				obj_result = new Gson().fromJson(resultSoap, Object_Logout.class);
			}
		} catch(Exception e){}

		return obj_result;
	}


	@Override
	public void onPostExecute(Object obj_result) {

		obj_prog.dismiss();
		try {
			if(obj_netwok_util.isInternetConnected(curr_context)){
				listener.onResultsSucceeded(obj_result);
			} else{
				obj_global.AlertMaterial_InternetNotConnected(curr_activity);
			}
		} catch(Exception e){
			System.out.println();
		}

	}
}