package com.asyncTasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import com.app.connection.WS_Calls;
import com.app.database.DatabaseManager;
import com.app.objects.Object_Login;
import com.app.objects.Object_Pickup_Request;
import com.global.Global;
import com.global.GlobalAasmStatus;
import com.global.network.NetworkUtil;
import com.global.network.TestConnection;
import com.global.ui.ui_ProgressDialog;
import com.global.ui.ui_ToastMessage;

public class Async_RequestDatabase extends AsyncTask<String, Integer, Object> implements GlobalAasmStatus {
	// Instances of current class
	Activity curr_activity;
	Context curr_context;
	/*
	 * Singelton Classes
	 */
	ui_ProgressDialog obj_prog = ui_ProgressDialog.getInstance();
	ui_ToastMessage obj_toast = ui_ToastMessage.getInstance();

	// Onject of TestConnection class
	TestConnection obj_connection = new TestConnection();

	WS_Calls obj_call_soap = new WS_Calls();

	AsyncReusable listener;
	private final NetworkUtil obj_netwok_util = new NetworkUtil();
	private final Global obj_global = new Global();

	public Async_RequestDatabase(final Activity curr_activity, final Context curr_context) {
		this.curr_activity = curr_activity;
		this.curr_context = curr_context;
	}

	public void setOnResultsListener(AsyncReusable listener) {
		this.listener = listener;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
//		obj_toast.makeToast(curr_activity, "Requesting Database");
	}

	@Override
	protected Object doInBackground(String... params) {
		try {
			List<Object_Pickup_Request> pickupRequestList = null;
			ArrayList<Object_Pickup_Request> pendingList = new ArrayList<Object_Pickup_Request>();
			ArrayList<Object_Pickup_Request> attemptedList = new ArrayList<Object_Pickup_Request>();
			String userId = Global.getLoginInfo(curr_activity).getObj_header().getUid();
			DatabaseManager databaseManager = DatabaseManager.getInstance(curr_context);
			Cursor cursor = databaseManager.getTodaysJob(userId);
			if (cursor != null && cursor.getCount() > 0) {
				pickupRequestList = databaseManager.getPickupRequestData(cursor);
			} else {
				// if not present in database then request api
				return AsyncReusable.DB_REQUEST_FAILED;
			}

			for (Object_Pickup_Request object_Pickup_Request : pickupRequestList) {
				if (object_Pickup_Request.getAasm_state().equals(KEY_STATUS_OUT_FOR_PICKUP)) {
					pendingList.add(object_Pickup_Request);
				} else {
					attemptedList.add(object_Pickup_Request);
				}
			}
			HashMap<String, ArrayList<Object_Pickup_Request>> map = new HashMap<String, ArrayList<Object_Pickup_Request>>();
			map.put("PENDING", pendingList);
			map.put("ATTEMPTED", attemptedList);
			return map;
		} catch (Exception e) {
			return AsyncReusable.DB_REQUEST_FAILED;
		}

	}

	@Override
	protected void onPostExecute(Object result) {
		super.onPostExecute(result);
		try {
			listener.onResultsSucceeded(result);
			if (!obj_netwok_util.isInternetConnected(curr_context)) {
				obj_global.AlertMaterial_InternetNotConnected(curr_activity);
			}
		} catch (Exception e) {
			System.out.println();
		}
	}

}
