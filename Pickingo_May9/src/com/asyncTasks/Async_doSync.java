package com.asyncTasks;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.app.connection.WS_Calls;
import com.app.database.DatabaseManager;
import com.app.objects.Object_Pickup_Request;
import com.app.pickingo.pickboy.R;
import com.global.Global;
import com.global.GlobalAasmStatus;
import com.global.UpdateProc;
import com.global.network.NetworkUtil;
import com.global.network.TestConnection;
import com.global.ui.ui_ProgressDialog;
import com.global.ui.ui_ToastMessage;

public class Async_doSync extends AsyncTask<String, Integer, Object> implements GlobalAasmStatus {

	// Instances of current class
	Activity curr_activity;
	Context curr_context;

	/*
	 * Singelton Classes
	 */
	ui_ProgressDialog obj_prog = ui_ProgressDialog.getInstance();
	ui_ToastMessage obj_toast = ui_ToastMessage.getInstance();

	// Onject of TestConnection class
	TestConnection obj_connection = new TestConnection();

	WS_Calls obj_call_soap = new WS_Calls();

	AsyncReusable listener;

	private final NetworkUtil obj_netwok_util = new NetworkUtil();
	private final Global obj_global = new Global();

	public Async_doSync(final Activity curr_activity, final Context curr_context) {

		this.curr_activity = curr_activity;
		this.curr_context = curr_context;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		obj_prog.makeProgressDialog(curr_activity, curr_context, true);
		obj_prog.prog.setMessage("Please Wait...");
		obj_prog.show();
	}

	public void setOnResultsListener(AsyncReusable listener) {
		this.listener = listener;
	}

	@Override
	protected Object doInBackground(String... params) {
		try {
			if (obj_netwok_util.isInternetConnected(curr_context)) {
				DatabaseManager databaseManager = DatabaseManager.getInstance(curr_context);
				ArrayList<Object_Pickup_Request> list = databaseManager.getDataForSync();
				int iCounter = 0;
				int iSize = list.size();
				if (iSize == 0)
					return curr_activity.getString(R.string.str_tst_already_sync);
				for (Object_Pickup_Request object_Pickup_Request : list) {
					Object_Pickup_Request object_New_Pickup_Request = UpdateProc.main_Update_Proc(curr_context, curr_activity, object_Pickup_Request);

					if (object_Pickup_Request.getAasm_state().equals(object_New_Pickup_Request.getAasm_state())) {
						String userId = Global.getLoginInfo(curr_activity).getObj_header().getUid();
						// sync the database
						if (databaseManager.updateSyncStatus(true, userId, object_Pickup_Request) == 1) {
							iCounter++;
						} else {
							Log.e("TAG", "sync status update failed");
						}
					}
				}
				return String.format(curr_activity.getString(R.string.str_tst_item_sync_counter), iCounter, iSize);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public void onPostExecute(Object obj_result) {

		obj_prog.dismiss();
		try {
			if (obj_netwok_util.isInternetConnected(curr_context)) {
				listener.onResultsSucceeded(obj_result);
			} else {
				obj_global.AlertMaterial_InternetNotConnected(curr_activity);
				// obj_global.AlertInternetNotConnected(curr_activity, false);
			}
			// obj_toast.makeToast(curr_activity, obj_result.toString());
		} catch (Exception e) {
			System.out.println();
		}

	}
}
