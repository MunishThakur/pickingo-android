package com.asyncTasks;

public interface AsyncReusable {
	public static final String API_REQUEST_FAILED = "FAILED";
	public static final String API_REQUEST_SUCCESS = "SUCCESS";
	public static final String DB_REQUEST_FAILED = "DB_FAILED";
	public static final String DB_REQUEST_SUCCESS = "DB_SUCCESS";

	public void onResultsSucceeded(Object result);
}
