/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package me.drakeet.materialdialog;

public final class R {
	public static final class color {
		public static final int card_background = 0x7f09000b;
		public static final int card_shadow = 0x7f09000c;
		public static final int lite_blue = 0x7f09000d;
		public static final int window_background = 0x7f09000e;
	}
	public static final class dimen {
		public static final int activity_horizontal_margin = 0x7f0a0002;
		public static final int activity_vertical_margin = 0x7f0a0003;
	}
	public static final class drawable {
		public static final int button = 0x7f020063;
		public static final int material_card = 0x7f020087;
		public static final int material_card_nos = 0x7f020088;
		public static final int material_card_nos_pressed = 0x7f020089;
		public static final int material_dialog_window = 0x7f02008a;
	}
	public static final class id {
		public static final int buttonLayout = 0x7f0b009f;
		public static final int contentView = 0x7f0b009d;
		public static final int empty_view = 0x7f0b00a0;
		public static final int material_background = 0x7f0b009c;
		public static final int message = 0x7f0b008c;
		public static final int message_content_view = 0x7f0b009e;
		public static final int title = 0x7f0b0037;
	}
	public static final class layout {
		public static final int layout_materialdialog = 0x7f040031;
	}
	public static final class string {
		public static final int action_settings = 0x7f0c007e;
		public static final int app_name = 0x7f0c0000;
		public static final int hello_world = 0x7f0c007d;
	}
}
